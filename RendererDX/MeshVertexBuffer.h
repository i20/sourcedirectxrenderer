#pragma once

#include "VertexBuffer.h"


class MeshVertexBuffer :	public VertexBuffer
{

	struct VertexTypeStandard
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 uv;
		D3DXVECTOR2 uv2;
		D3DXVECTOR3 Normal;
		D3DXVECTOR3 Tangent;
		D3DXVECTOR3 Bitangent;
		D3DXVECTOR4 Color;
	};


#pragma region methods

public:

	MeshVertexBuffer(std::string meshFileName);
	~MeshVertexBuffer();

	bool Initialize(ID3D11Device* device) override;

private:
	void SizeOfBuffer() override;
	bool CheckMeshData(void* vec, const char* dataType ,const char* name);
	bool CreateFallBackGeo(ID3D11Device* device);

#pragma endregion methods

#pragma region members

	std::string m_filePath;
	VertexTypeStandard* m_vertices;
	void* m_fallbackMeshData;

#pragma endregion members

public:
	void Render(ID3D11DeviceContext* deviceContext) override;

};

