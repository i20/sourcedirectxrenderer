#include "pch.h"

#include <string.h>
#include <iostream>
#include <vector>

#include "MeshVertexBuffer.h"
#include "OpenFBX/ofbx.h"

MeshVertexBuffer::MeshVertexBuffer(std::string meshFileName)
{
	m_fallbackMeshData = nullptr;
	m_filePath = meshFileName;
}


MeshVertexBuffer::~MeshVertexBuffer()
{
	if (m_fallbackMeshData)
	{
		delete[] m_fallbackMeshData;
		m_fallbackMeshData = nullptr;
	}
}

bool MeshVertexBuffer::Initialize(ID3D11Device* device)
{
	if (m_filePath.empty())
	{
		CreateFallBackGeo(device);
		return true;
	}

	const char* filepath = m_filePath.c_str();

	FILE *fp;
	errno_t err;

	// disable this warning because we want to use unsafe version of strerror
	#pragma warning(disable:4996) 

	if ((err = fopen_s(&fp, filepath, "rb")) != 0)
	{
		std::cout << fprintf(stderr, "cannot open file '%s': %s\n", filepath, strerror(err)) << std::endl;
		return false;
	}

	fseek(fp, 0, SEEK_END);
	long file_size = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	auto* content = new ofbx::u8[file_size];
	fread(content, 1, file_size, fp);

	ofbx::IScene* g_scene = nullptr;

	g_scene = ofbx::load((ofbx::u8*)content, file_size, (ofbx::u64)ofbx::LoadFlags::TRIANGULATE);

	if (!g_scene) {
		std::cout << "error loading file " << std::endl;
		return false;
	}

	const ofbx::Mesh* recievedMesh = g_scene->getMesh(0);
	const char* meshName = recievedMesh->name;
	const ofbx::Geometry* recievedGeometry = recievedMesh->getGeometry();

	m_vertexCount = recievedGeometry->getVertexCount();
	m_indexCount = recievedGeometry->getIndexCount();

	// fallback data to 0 in case something is missing from the fbx

	ofbx::Vec4 zeroVec = ofbx::Vec4{ 0.0f, 0.0f, 0.0f, 1.0f };
	ofbx::Vec4* fallbackMeshData = new ofbx::Vec4[m_vertexCount];

	for (int i = 0; i < m_vertexCount; i++)
	{
		fallbackMeshData[i] = zeroVec;
	}

	m_fallbackMeshData = fallbackMeshData;


	m_vertices = new VertexTypeStandard[m_vertexCount];
	m_indices = new unsigned long[m_indexCount];

	// indices

	const int* intptr = recievedGeometry->getFaceIndices();
	for (int i = 0; i < m_indexCount; i++)
	{
		m_indices[i] = intptr[i];

		if (intptr[i] < 0)
		{
			m_indices[i] = abs(intptr[i]) - 1;
		}
	}

	// position

	const ofbx::Vec3* vector3 = recievedGeometry->getVertices();
	if (!CheckMeshData((void*)vector3, (char*) "position", meshName))
	{
		vector3 = (ofbx::Vec3*)m_fallbackMeshData;
	}
	for (int i = 0; i < m_vertexCount; i++)
	{
		m_vertices[i].position.x = (float)vector3[i].x;
		m_vertices[i].position.y = (float)vector3[i].y;
		m_vertices[i].position.z = (float)vector3[i].z;
	}

	// uvs

	const ofbx::Vec2* vector2 = recievedGeometry->getUVs(0);
	if (!CheckMeshData((void*)vector2, (char*)"uv 1", meshName))
	{
		vector2 = (ofbx::Vec2*)m_fallbackMeshData;
	}
	for (int i = 0; i < m_vertexCount; i++)
	{
		m_vertices[i].uv.x = (float)vector2[i].x;
		m_vertices[i].uv.y = (float)vector2[i].y;
	}

	vector2 = recievedGeometry->getUVs(1);
	if (!CheckMeshData((void*)vector2, (char*)"uv 2", meshName))
	{
		vector2 = (ofbx::Vec2*)m_fallbackMeshData;
	}
	for (int i = 0; i < m_vertexCount; i++)
	{
		m_vertices[i].uv2.x = (float)vector2[i].x;
		m_vertices[i].uv2.y = (float)vector2[i].y;
	}

	const ofbx::Vec4* vector4 = recievedGeometry->getColors();
	if (!CheckMeshData((void*)vector4, (char*)"vertexColor", meshName))
	{
		vector4 = (ofbx::Vec4*)m_fallbackMeshData;
	}
	for (int i = 0; i < m_vertexCount; i++)
	{
		m_vertices[i].Color.x = (float)vector4[i].x;
		m_vertices[i].Color.y = (float)vector4[i].y;
		m_vertices[i].Color.z = (float)vector4[i].z;
		m_vertices[i].Color.w = (float)vector4[i].w;

	}

	// Normals

	vector3 = recievedGeometry->getNormals();
	if (!CheckMeshData((void*)vector3, (char*)"normal", meshName))
	{
		vector3 = (ofbx::Vec3*)m_fallbackMeshData;
	}
	for (int i = 0; i < m_vertexCount; i++)
	{
		m_vertices[i].Normal.x = (float)vector3[i].x;
		m_vertices[i].Normal.y = (float)vector3[i].y;
		m_vertices[i].Normal.z = (float)vector3[i].z;
	}

	// Tangent Bitangent

	D3DXVECTOR3 bitangent;

	vector3 = recievedGeometry->getTangents();
	if (!CheckMeshData((void*)vector3, (char*)"tangent", meshName))
	{
		vector3 = (ofbx::Vec3*)m_fallbackMeshData;
	}
	
	for (int i = 0; i < m_vertexCount; i++)
	{
		m_vertices[i].Tangent.x = (float)vector3[i].x;
		m_vertices[i].Tangent.y = (float)vector3[i].y;
		m_vertices[i].Tangent.z = (float)vector3[i].z;

		D3DXVec3Cross(&bitangent, &m_vertices[i].Normal, &m_vertices[i].Tangent);

		m_vertices[i].Bitangent = bitangent;
	}

	m_verticesPtr = (void*)m_vertices;
	bool result = VertexBuffer::Initialize(device);

	m_name = meshName;

	delete[] content;
	fclose(fp);
	
	return result;
}

void MeshVertexBuffer::SizeOfBuffer()
{
	m_bufferSize = sizeof(VertexTypeStandard);
}

bool MeshVertexBuffer::CheckMeshData(void* vec, const char* dataType, const char* name)
{
	if (vec == nullptr)
	{
		std::cout << "data " << dataType << " missing on : " << name << std::endl;
		return false;
	}
	else 
	{
		return true;
	}
}

bool MeshVertexBuffer::CreateFallBackGeo(ID3D11Device* device)
{
	m_name = "default";
	m_vertexCount = 4;
	m_indexCount = 6;

	m_vertices = new VertexTypeStandard[m_vertexCount];
	m_indices = new unsigned long[m_indexCount];

	const float defaultPlaneSize = 100.0f;

	m_vertices[0].position = D3DXVECTOR3(-1.0f * defaultPlaneSize, -1.0f * defaultPlaneSize, 0.0f);
	m_vertices[1].position = D3DXVECTOR3(-1.0f * defaultPlaneSize, 1.0f * defaultPlaneSize, 0.0f);
	m_vertices[2].position = D3DXVECTOR3(1.0f * defaultPlaneSize, 1.0f * defaultPlaneSize, 0.0f);
	m_vertices[3].position = D3DXVECTOR3(1.0f * defaultPlaneSize, -1.0f * defaultPlaneSize, 0.0f);

	m_vertices[0].uv = D3DXVECTOR2(0.0f, 1.0f);
	m_vertices[1].uv = D3DXVECTOR2(0.0f, 0.0f);
	m_vertices[2].uv = D3DXVECTOR2(1.0f, 0.0f);
	m_vertices[3].uv = D3DXVECTOR2(1.0f, 1.0f);

	m_indices[0] = 0;
	m_indices[1] = 1;
	m_indices[2] = 2;
	m_indices[3] = 0;
	m_indices[4] = 2;
	m_indices[5] = 3;

	m_verticesPtr = (void*)m_vertices;
	bool result = VertexBuffer::Initialize(device);
	return result;

}

void MeshVertexBuffer::Render(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	stride = m_bufferSize;
	offset = 0;

	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

}

