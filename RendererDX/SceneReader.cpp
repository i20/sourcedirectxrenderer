#include "pch.h"
#include "SceneReader.h"
#include <d3d11.h>


#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>

#include "Material.h"
#include "Texture.h"
#include "Shader.h"
#include "EntityManager.h"
#include "Entity.h"

#include "RessourceManager.h"
#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"	

SceneReader::SceneReader()
{

}

SceneReader::~SceneReader()
{

}
void SceneReader::ReadSceneResources(class RessourceManager* resourceManager, class EntityManager* entityManager, std::string path, ID3D11DeviceContext* deviceContext)
{
	using namespace rapidjson;


	std::ifstream targetFile;
	std::string loadedDocument;
	std::string line;


	targetFile.open(path);

	if (targetFile.is_open())
	{
		while (std::getline(targetFile, line))
		{
			loadedDocument.append(line.c_str());
			loadedDocument.append("\n");

		}
	}
	else
	{
		std::cout << "Unable to open file : " << path << " \n ";
	}

	targetFile.close();

	Document document;
	document.Parse(loadedDocument.c_str());
	

	if (!document.IsObject())
	{
		size_t error = document.GetErrorOffset();
		targetFile.open(path);
		int lineCounter = 0;
		std::string currentChar;

		for (size_t Index = 0; Index < error; Index++)
		{
			currentChar = targetFile.get();
			if (currentChar == "\n")
				lineCounter++;
		}

		targetFile.close();

		std::cout << "error reading Json file !!! \n " << "at or before line number : " << (int)lineCounter << "\n \n" ;
		return;
	}

	if (!document.HasMember("scene"))
	{
		return;
	}
	
	Value& Scene = document["scene"];

	// load textures 

	if (Scene.HasMember("textures"))
	{
		Value& textures = Scene["textures"];
		for (SizeType i = 0; i < textures.Size(); i++)
		{
			if (textures[i].HasMember("path"))
			{
				resourceManager->LoadTextureRessource((LPCSTR)textures[i]["path"].GetString());
			}
		}
	}

	// load shaders 

	if (Scene.HasMember("shaders"))
	{
		Value& shaders = Scene["shaders"];
		for (SizeType i = 0; i < shaders.Size(); i++)
		{
			if (shaders[i].HasMember("path"))
			{
				resourceManager->LoadShaderRessource( (LPCTSTR)shaders[i]["path"].GetString());
			}
		}
	}

	// load meshes 

	if (Scene.HasMember("meshes"))
	{
		Value& meshes = Scene["meshes"];
		for (SizeType i = 0; i < meshes.Size(); i++)
		{
			if (meshes[i].HasMember("path"))
			{
				resourceManager->LoadGeometryRessource(meshes[i]["path"].GetString());
			}
		}
	}

	// create materials 
	if (Scene.HasMember("materials"))
	{
		Value& materials = Scene["materials"];
		for (SizeType i = 0; i < materials.Size(); i++)
		{
			Material* mat;
			if (materials[i].HasMember("name"))
			{
				mat = resourceManager->CreateMaterialRessource((char*)materials[i]["name"].GetString());
			}
			else 
			{
				continue;
				std::cout << "could not find \"name\" in  material file \n";
			}

			// floats 
			if (materials[i].HasMember("floatParameters"))
			{
				Value& parameters = materials[i]["floatParameters"];
				for (SizeType o = 0; o < parameters.Size(); o++)
				{
					if (parameters[o].HasMember("parameterName") && parameters[o].HasMember("value"))
					{
						mat->SetMaterialParameter(parameters[o]["parameterName"].GetString(), parameters[o]["value"].GetFloat());
					}
				}
				
			}
			
			// vectors  
			if (materials[i].HasMember("vectorParameters") )
			{
				Value& parameters = materials[i]["vectorParameters"];
				for (SizeType o = 0; o < parameters.Size(); o++)
				{
					if (parameters[o].HasMember("parameterName") && parameters[o].HasMember("value"))
					{
						std::vector<float> vec;
						for (SizeType v = 0; v < parameters[o].Size(); v++)
						{
							vec.push_back(parameters[o]["value"][v].GetFloat());
						}
						
						mat->SetMaterialParameter(parameters[o]["parameterName"].GetString(), vec);
					}
				}
			}

			
			// color  
			if (materials[i].HasMember("colorParameters"))
			{
				Value& parameters = materials[i]["colorParameters"];
				for (SizeType o = 0; o < parameters.Size(); o++)
				{
					if (parameters[o].HasMember("parameterName") && parameters[o].HasMember("value"))
					{
						std::vector<float> vec;
						for (SizeType v = 0; v < parameters[o]["value"].Size(); v++)
						{
							vec.push_back(parameters[o]["value"][v].GetFloat());
						}
					
						mat->SetMaterialParameter(parameters[o]["parameterName"].GetString(), vec);
					}
				}
			}
			
			// textures
			if (materials[i].HasMember("textureParameters"))
			{
				Value& parameters = materials[i]["textureParameters"];
				for (SizeType o = 0; o < parameters.Size(); o++)
				{
					if (parameters[o].HasMember("parameterName") && parameters[o].HasMember("textureName"))
					{
						mat->SetMaterialParameter((std::string)parameters[o]["parameterName"].GetString(), resourceManager, (std::string)parameters[o]["textureName"].GetString());
					}
				}

			}

			// shader 
			if (materials[i].HasMember("shader"))
			{
				mat->SetShader(resourceManager->GetShaderByName((char*)materials[i]["shader"].GetString()), resourceManager);
			}
		}
	}

	// entities 

	ID3D11Device* device = resourceManager->GetDevice();
	if (Scene.HasMember("entities"))
	{
		Value& parameters = Scene["entities"];
		for (SizeType o = 0; o < parameters.Size(); o++)
		{
			Value& workEntity = parameters[o];
			if (workEntity.HasMember("type") && workEntity.HasMember("material"))
			{
				Entity* entity = entityManager->AddEntity();
				Material* mat = resourceManager->GetMaterialByName((char*)workEntity["material"].GetString());

				std::string type = workEntity["type"].GetString();
				if (type.compare(std::string("staticMesh")) == 0)
				{
					if (workEntity.HasMember("mesh"))
					{
						entity->Initialise(device, mat, resourceManager->GetGeoByName((char*)workEntity["mesh"].GetString()));

					}
					else
					{
						continue;
					}
				}
				else
				{
					if (type.compare(std::string("animatedSprite")) == 0)
					{
						float size = 100.0f;
						bool isLooping = true;
						float speed = 1.0f;
						float numberOfFrames = 4.0f;
						std::string animatedTexture = "";
						if (workEntity.HasMember("animatedTexture"))
						{
							animatedTexture = workEntity["animatedTexture"].GetString();
						}
						if (workEntity.HasMember("size"))
						{
							size = workEntity["size"].GetFloat();
						}
						if (workEntity.HasMember("looping"))
						{
							isLooping = workEntity["looping"].GetBool();
						}
						if (workEntity.HasMember("speed"))
						{
							speed = workEntity["speed"].GetFloat();
						}
						if (workEntity.HasMember("framesCount"))
						{
							numberOfFrames = workEntity["framesCount"].GetFloat();
						}

						entity->Initialise(device, deviceContext, mat, size, numberOfFrames, speed, animatedTexture, isLooping);


					}
					else
					{
						if (type.compare(std::string("sprite")) == 0)
						{
							float size = 100.0f;
							if (workEntity.HasMember("size"))
							{
								size = workEntity["size"].GetFloat();
							}
							entity->Initialise(device, mat, size);
						}
						else
						{
							continue;
						}
					}
				}

				if (workEntity.HasMember("position"))
				{
					if (workEntity["position"].IsArray())
					{
						Value& entityPos = workEntity["position"].GetArray();
						if (entityPos.Size() > 2)
						{
							entity->SetPostion(entityPos[0].GetFloat(), entityPos[1].GetFloat(), entityPos[2].GetFloat());
						}
						else 
						{
							if (entityPos.Size() > 1)
							{
								entity->SetPostion(entityPos[0].GetFloat(), entityPos[1].GetFloat());

							}
							else
							{
								entity->SetPostion(entityPos[0].GetFloat());
							}
						}
					}
				}

				if (workEntity.HasMember("rotation"))
				{
					if (workEntity["rotation"].IsArray())
					{
						Value& entityPos = workEntity["rotation"].GetArray();
						if (entityPos.Size() > 2)
						{
							entity->SetRotation(entityPos[0].GetFloat(), entityPos[1].GetFloat(), entityPos[2].GetFloat());

						}
						else
						{
							if (entityPos.Size() > 1)
							{
								entity->SetRotation(entityPos[0].GetFloat(), entityPos[1].GetFloat() ) ;
							}
							else
							{
								entity->SetRotation((entityPos[0].GetFloat() ) );
							}
						}
					}
				}

			}
		}
	}
}