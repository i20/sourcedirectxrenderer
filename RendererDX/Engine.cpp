
#include "pch.h"

#include <math.h>
#include <iostream>
#include <string.h>

#include "Graphics.h"
#include "Entity.h"
#include "EntityManager.h"
#include "UserInput.h"
#include "RessourceManager.h"
#include "Camera.h"
#include "CameraController.h"
#include "Material.h"
#include "Shader.h"
#include "SceneReader.h"

#include "Engine.h"

Engine* Engine::m_engineInstance = NULL;

Engine::Engine()
{
	m_graphics = NULL;
	m_resourceManager = NULL;
	m_camera = NULL;
	m_entityManager = NULL;
	m_input = NULL;
	m_cameraController = NULL;
}

Engine::~Engine()
{
	if (m_graphics)
	{
		delete m_graphics;
		m_graphics = nullptr;
	}

	if (m_camera)
	{
		delete m_resourceManager;
		m_resourceManager = nullptr;
	}
	
	if (m_resourceManager)
	{
		delete m_resourceManager;
		m_resourceManager = nullptr;
	}

	if (m_entityManager)
	{
		delete m_entityManager;
		m_entityManager = nullptr;
	}

	if (m_input)
	{
		delete m_input;
		m_input = nullptr;
	}

	if (m_cameraController)
	{
		delete m_cameraController;
		m_cameraController = nullptr;
	}

	if (m_sceneReader)
	{
		delete m_sceneReader;
		m_sceneReader = nullptr;
	}
}


bool Engine::InitializeGraphics(HWND hwnd)
{
	m_entityManager = EntityManager::GetInstance();
	m_graphics = new Graphics();
	return m_graphics->InitializeDX(hwnd);

}

bool Engine::Initialize(HINSTANCE histance, HWND hwnd)
{
	m_input = new UserInput();
	if (!m_input->Initialize(histance, hwnd, SCREEN_WIDTH, SCREEN_HEIGHT))
	{
		return false;
	}
	m_input->Update();


	m_cameraController = new CameraController();
	m_camera = m_cameraController->Initialize();

	m_graphics->Initialize();
	m_graphics->GetHwnd();

	m_resourceManager = RessourceManager::GetInstance();
	m_resourceManager->SetDevice(m_graphics->GetDevice(), hwnd);

	m_sceneReader = new SceneReader();
	m_sceneReader->ReadSceneResources(m_resourceManager, m_entityManager, "levels/level.json", m_graphics->GetDeviceContext());

	return true; 
}

void Engine::Run()
{
	Update();
	Render();
}

void Engine::Release()
{
	if (m_engineInstance)
	{
		delete m_engineInstance;
		m_engineInstance = NULL;
	}
}

Engine* Engine::GetEngine()
{
	if (m_engineInstance == NULL)
	{
		m_engineInstance = new Engine();
	}
	return m_engineInstance;
}

Graphics* Engine::GetGraphics()
{
	return m_graphics;
}

UserInput* Engine::GetInput()
{
	return m_input;
}

void Engine::Update()
{

	m_entityManager->Update();
	m_input->Update();
	m_cameraController->Update();

}

void Engine::Render()
{
	m_camera->Update();
	m_graphics->BeginScene(0.0f, 0.0f, 0.5f, 1.0f);

	//render stuff goes here

	D3DXMATRIX viewMatrix = m_camera->GetViewMatrix(); 
	D3DXMATRIX projMatrix = m_camera->GetProjectionMatrix();;

	m_entityManager->Render(m_graphics->GetDeviceContext(), viewMatrix, projMatrix);

	m_graphics->EndScene();
}
