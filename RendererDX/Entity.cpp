#include "pch.h"

#include "Entity.h"
#include "Timer.h"
#include "AnimatedSprite.h"
#include "StaticMesh.h"
#include "MeshVertexBuffer.h"
#include "Material.h"


Entity::Entity()
{
	m_position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	D3DXMatrixIdentity(&m_worldMatrix);
	m_renderableObject = NULL;
}


Entity::~Entity()
{
	if (m_renderableObject)
	{
		m_renderableObject = NULL;
	}
}

// static mesh type entity
void Entity::Initialise(ID3D11Device* device, Material* material, MeshVertexBuffer* buffer)
{
	m_renderableObject = new StaticMesh();
	((StaticMesh*)m_renderableObject)->SetMeshVertexBuffer(buffer);
	((StaticMesh*)m_renderableObject)->Initialise(device, material);
}

// sprite type entity
void Entity::Initialise(ID3D11Device* device, Material* material, float spriteSize)
{
	m_renderableObject = new Sprite(spriteSize);
	((Sprite*)m_renderableObject)->Initialise(device, material);
}

// animated sprite type entity
void Entity::Initialise(ID3D11Device* device, ID3D11DeviceContext* deviceContext, Material* material, float spriteSize, float frammesPerSecond, float animationSpeed, std::string animatedTexture, bool isLooping /*= true*/)
{
	m_renderableObject = new AnimatedSprite(spriteSize, frammesPerSecond, animationSpeed, isLooping);
	((AnimatedSprite*)m_renderableObject)->Initialise(device, deviceContext, material, animatedTexture);
}

void Entity::Update()
{
	D3DXMatrixTranslation(&m_worldMatrix, m_position.x, m_position.y, m_position.z);
	D3DXMATRIX rotationMatrix;
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, m_rotation.y, m_rotation.x, m_rotation.z);

	//m_worldMatrix = rotationMatrix;

	D3DXMatrixMultiply(&m_worldMatrix, &rotationMatrix, &m_worldMatrix );
	//D3DXMatrix

	if (m_renderableObject)
	{
		m_renderableObject->Update();
	}
}

void Entity::Render(ID3D11DeviceContext* deviceContext, D3DMATRIX viewMatrix, D3DMATRIX projectionMatrix)
{
	if (m_renderableObject)
	{
		m_renderableObject->Render(deviceContext, m_worldMatrix, viewMatrix, projectionMatrix);
	}
}

void Entity::SetPostion(float x /*= 0.0f*/, float y /*= 0.0f*/, float z /*= 0.0f*/)
{
	m_position.x = x;
	m_position.y = y;
	m_position.z = z;
}

void Entity::SetPostion(D3DXVECTOR3 vector)
{
	m_position = vector;
}

void Entity::SetRotation(float x /*= 0.0f*/, float y /*= 0.0f*/, float z /*= 0.0f*/)
{	
	m_rotation.x = x * (float)D3DX_PI / 180.0f;
	m_rotation.y = y * (float)D3DX_PI / 180.0f;
	m_rotation.z = z * (float)D3DX_PI / 180.0f;

}

void Entity::SetRotation(D3DXVECTOR3 vector)
{
	m_rotation = vector;
}
