#include "pch.h"

#include "SpriteVertexBuffer.h"


SpriteVertexBuffer::SpriteVertexBuffer(float size, bool writable)
{
	m_size = 0.0f;
	m_writable = writable;
	m_size = size;
}

SpriteVertexBuffer::~SpriteVertexBuffer()
{
}

bool SpriteVertexBuffer::Initialize(ID3D11Device* device)
{
	m_vertexCount = 4;
	m_indexCount = 6;

	m_vertices = new VertexTypeBase[m_vertexCount];
	m_indices = new unsigned long[m_indexCount];

	m_vertices[0].position = D3DXVECTOR3(-1.0f * m_size, -1.0f * m_size, 0.0f);
	m_vertices[1].position = D3DXVECTOR3(-1.0f * m_size, 1.0f * m_size, 0.0f);
	m_vertices[2].position = D3DXVECTOR3(1.0f * m_size, 1.0f * m_size, 0.0f);
	m_vertices[3].position = D3DXVECTOR3(1.0f * m_size, -1.0f * m_size, 0.0f);

	m_vertices[0].uv = D3DXVECTOR2(0.0f, 1.0f);
	m_vertices[1].uv = D3DXVECTOR2(0.0f, 0.0f);
	m_vertices[2].uv = D3DXVECTOR2(1.0f, 0.0f);
	m_vertices[3].uv = D3DXVECTOR2(1.0f, 1.0f);

	m_vertices[0].uv2 = D3DXVECTOR2(0.0f, 2.0f);
	m_vertices[1].uv2 = D3DXVECTOR2(0.0f, 0.0f);
	m_vertices[2].uv2 = D3DXVECTOR2(2.0f, 0.0f);
	m_vertices[3].uv2 = D3DXVECTOR2(2.0f, 2.0f);

	m_indices[0] = 0;
	m_indices[1] = 1;
	m_indices[2] = 2;
	m_indices[3] = 0;
	m_indices[4] = 2;
	m_indices[5] = 3;
	
	m_verticesPtr = (void*)m_vertices;
	bool result = VertexBuffer::Initialize(device);

	return result;
}


SpriteVertexBuffer::VertexTypeBase* SpriteVertexBuffer::GetVertices()
{
	return m_vertices;
}