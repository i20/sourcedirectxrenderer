#pragma once

#include <d3d10_1.h>
#include <d3d10.h>
#include <D3DX10math.h>
#include <d3d11.h>

class RenderableObject
{

#pragma region methods

public:
	RenderableObject();
	virtual ~RenderableObject(void);

	virtual void Update(){} 
	virtual void Render(ID3D11DeviceContext* deviceContext, D3DMATRIX worldMatrix, D3DMATRIX viewMatrix, D3DMATRIX projectionMatrix)=0;
	virtual void Initialise(ID3D11Device* device, class Material* material);

#pragma endregion methods


#pragma region members

protected:
	class VertexBuffer* m_vertexBuffer;
	class Shader* m_shader;
	class Material* m_material;

#pragma endregion members


};