#pragma once

#include <vector>
#include <D3DX10math.h>

class EntityManager
{

#pragma region methods

public:
	~EntityManager();

	void Update();
	void Render(ID3D11DeviceContext* deviceContext, D3DXMATRIX viewMatric, D3DXMATRIX projectionMatrix);

	class Entity* AddEntity();
	void RemoveEntity(Entity* entity);
	void RemoveAllEntities();
	bool ContainsEntity(Entity* entity);
	bool ContainsEntity(Entity* entity, int& index);

	static EntityManager* GetInstance();

#pragma endregion methods

#pragma region members

private:
	EntityManager(void);
	std::vector<Entity*> m_entities;
	static EntityManager* m_instance;

#pragma endregion members
};

