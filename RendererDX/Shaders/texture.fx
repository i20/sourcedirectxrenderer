
/*
	markings :
	shader compiler will check for all cbuffer and register number to create parameters
	then look for #define to special markings
	avaliable Markinsg are :
	VERTEX_SHADER
	FRAGMENT_SHADER
	INPUT

	for input theres is 3 avaliable
	base
	standard
	full

	the compiler also look for macro to get material definition
	PSUNIFORM
	VSUNIFORM
*/


#define VERTEX_SHADER vertShader  
#define FRAGMENT_SHADER FragmentShader 
#define INPUT base 

#define VSUNIFORM( NUM, TYPE, NAME) \
cbuffer VS_CONSTANT_BUFFER  : register(b##NUM)\
{\
	TYPE NAME;\
};\

#define PSUNIFORM( NUM, TYPE, NAME) \
cbuffer PS_CONSTANT_BUFFER  : register(b##NUM)\
{\
	TYPE NAME;\
};\

// vertex data

cbuffer VS_CONSTANT_BUFFER : register(b0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

PSUNIFORM(7, float4, colorMultiplySecond)


PSUNIFORM(1, float4, colorMultiply)


// strucs

struct vertexInput
{
	float4 position : POSITION;
	float2 uv : TEXCOORD0;
	float2 uv2 : TEXCOORD1;
};

struct pixelInput
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD0;
	float4 co : TEXCOORD1;
};

Texture2D coucou;
//Texture2D machin;
Texture2D shaderTexture;
SamplerState SamplerType;


// vertex shader 

pixelInput vertShader(vertexInput input)
{
	pixelInput output;

	input.position.w = 1.0;

	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	output.uv = input.uv;
	//output.uv.x = 1.0 - output.uv.x;
	output.uv.y = 1.0 - output.uv.y;

	//output.co = (colorMultiply + colorMultiplySecond);
	output.co = float4(1.0, 1.0, 1.0, 1.0);
	return output;
}

// pixel shader

float4 FragmentShader(pixelInput input) : SV_TARGET
{
	//float4 colorA = coucou.SampleLevel(SamplerType, input.uv, 0, 0);
	float4 colorA = shaderTexture.Sample(SamplerType, input.uv);

	float4 colorB = shaderTexture.Sample(SamplerType, input.uv);

	float3 colorFinale = lerp(colorA.xyz, colorB.xyz, step(input.uv.x, 0.5));
	//return float4(colorFinale, 1.0);
	return float4(colorFinale * (colorMultiply.xyz + colorMultiplySecond.xyz), 1.0);
	//return float4(colorFinale * input.co.xyz, 1.0);
}