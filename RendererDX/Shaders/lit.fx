
/* 
	markings : 
	shader compiler will check for all cbuffer and register number to create parameters 
	then look for #define to special markings 
	avaliable Markinsg are : 
	VERTEX_SHADER
	FRAGMENT_SHADER
	INPUT
	
	for input theres is 3 avaliable
	base
	standard
	full

	the compiler also look for macro to get material definition
	PSUNIFORM
	VSUNIFORM
*/


#define VERTEX_SHADER vertShader  
#define FRAGMENT_SHADER FragmentShader 
#define INPUT standard

#define VSUNIFORM( NUM, TYPE, NAME) \
cbuffer VS_CONSTANT_BUFFER  : register(b##NUM)\
{\
	TYPE NAME;\
};\

#define PSUNIFORM( NUM, TYPE, NAME) \
cbuffer PS_CONSTANT_BUFFER  : register(b##NUM)\
{\
	TYPE NAME;\
};\

// vertex data

	cbuffer VS_CONSTANT_BUFFER : register(b0)
	{
		matrix worldMatrix;
		matrix viewMatrix;
		matrix projectionMatrix;
	};

	PSUNIFORM(2, float4, lightColor)

	PSUNIFORM(1 , float4, shadowColor)


// strucs
/*
struct vertexInput
{
	float4 position : POSITION;
	float2 uv : TEXCOORD0;
	float2 uv2 : TEXCOORD1;
	float3 normal : NORMAL;

};
	*/


	struct vertexInput
	{
		float4 position : POSITION;
		float2 uv : TEXCOORD0;
		float2 uv2 : TEXCOORD1;
		float3 normal : NORMAL;
		float3 tangent : TANGENT;
		float3 bitangent : BINORMAL;
		float4 color : COLOR;
	};



struct pixelInput
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD0;
	float4 normal : TEXCOORD1;
	float3 tangentLightDir: TEXCOORD2;
};

Texture2D normalTexture;

Texture2D coucou;
//Texture2D machin;
Texture2D shaderTexture;



SamplerState SamplerType;


// vertex shader 

pixelInput vertShader( vertexInput input)
{
	pixelInput output;

	input.position.w = 1.0;

	output.position = mul( input.position, worldMatrix);
	output.position = mul( output.position, viewMatrix);
	output.position = mul( output.position, projectionMatrix);

	output.uv = input.uv;
	output.uv.y = 1.0 - output.uv.y;


	float3 normal = mul(float4(input.normal.xyz, 0.0), worldMatrix).xyz;
	float3 tangent = mul(float4(input.tangent.xyz, 0.0), worldMatrix).xyz;
	float3 binormal = mul( float4(input.bitangent.xyz, 0.0), worldMatrix).xyz;

	float3x3 worldToTangent = float3x3 (tangent, binormal, normal);

	float3 lightDir = normalize(float3(0.5, 0.7, -0.1));

	lightDir = mul(worldToTangent, lightDir);
	output.tangentLightDir = lightDir;

	output.normal = float4(normal.xyz, 0.0);
	
	return output;
}

// pixel shader

float4 FragmentShader(pixelInput input) : SV_TARGET
{
	//float4 colorA = coucou.SampleLevel(SamplerType, input.uv, 0, 0);
	float4 colorA = shaderTexture.Sample(SamplerType, input.uv);
	colorA = pow(colorA, 2.2);// * colorA;

	float4 colorB = shaderTexture.Sample(SamplerType, input.uv);
	colorB = pow(colorB, 2.2);// *colorB;
	
	float2 normalChannel = normalTexture.Sample(SamplerType, input.uv).yw;
	float3 normal = float3(normalChannel.y, normalChannel.x, 1.0);
	normal.z = sqrt( (normal.x + normal.x) + (normal.y + normal.y) );
	normal.y = 1.0 - normal.y;
	//normal = normalize(normal);
	normal = normal * 2.0 - 1.0;
	
	/*
	// version that do not hack channels 
	float3 normal = normalTexture.Sample(SamplerType, input.uv).xyz;
	normal.y = 1.0 - normal.y;
	normal = normal * 2.0 - 1.0;
	*/

	float3 colorFinale = lerp(colorA.xyz, colorB.xyz, step(input.uv.x, 0.5));
	
	float lighting = saturate(dot(normal.xyz, input.tangentLightDir));
	lighting = saturate(lighting);

	float3 lightcolor = lerp(shadowColor.xyz * .55, lightColor.xyz * 1.25, saturate(lighting));
	
    return sqrt( float4(colorFinale * lightcolor, 1.0) );
	//return float4(colorFinale * input.co.xyz, 1.0);
}