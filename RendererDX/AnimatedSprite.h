#pragma once

#include "Sprite.h"
#include <string.h>

class AnimatedSprite :	public Sprite
{

#pragma region methods 

public:
	AnimatedSprite(float size, float frammesPerSecond, float animationSpeed = 1.0f, bool isLooping = true);
	~AnimatedSprite(void);

	void Initialise(ID3D11Device* device, ID3D11DeviceContext* deviceContext, Material* material, std::string animatedTexture);
	void Render(ID3D11DeviceContext* deviceContext, D3DMATRIX worldMatrix, D3DMATRIX viewMatrix, D3DMATRIX projectionMatrix) override;
	void Update();

#pragma endregion methods 


#pragma region members

private:
	ID3D11DeviceContext* m_deviceContext;
	float m_currentFrame;
	float m_previousFrame;
	float m_maxFrames;
	float m_animationSpeed;
	float m_currentSpeed;
	float m_rammesPerSecond;
	bool m_looping;
	class Texture* m_texture;


#pragma endregion members


};

