#include "pch.h"

#include "SystemDefs.h"
#include "UserInput.h"
#include "Engine.h"
#include "Camera.h"
#include  <iostream>

#include "CameraController.h"


CameraController::CameraController()
{
	m_camera = NULL;
	m_movementSpeed = 5.6f;
	m_fovAngle = 0.45f;
}


CameraController::~CameraController()
{
	if (m_camera)
	{
		delete m_camera;
		m_camera = NULL;
	}
}

Camera* CameraController::Initialize()
{
	m_camera = new Camera();
	// in radian D3DX_PI * 0.33f  is 60�
	// in radian D3DX_PI * 0.5f  is 90�
	// in radian D3DX_PI * 0.66f  is 120�
	m_camera->InitializeProjectionMatrix((float)D3DX_PI * m_fovAngle, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, SCREEN_NEAR, SCREEN_DEPTH);
	m_camera->InitializeOrthoMatrix(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_NEAR, SCREEN_DEPTH);

	m_camera->SetPosition(0.0f, 0.0f, -350.0f);
	m_camera->Update();

	return m_camera;
}

void CameraController::Update()
{
	UpdateInput();
	m_camera->Update();
}

void CameraController::UpdateInput()
{

	UserInput* input = Engine::GetEngine()->GetInput();
	if (input == NULL)
	{
		return;
	}

	if (input->IsKeyDown(DIK_W))
	{
		D3DXVECTOR3 pos = m_camera->GetPosition();
		pos = pos + m_camera->GetForwardVector() * m_movementSpeed;
		m_camera->SetPosition(pos.x, pos.y, pos.z);
		//std::cout << pos.x << ", " << pos.y << " , " << pos.z << std::endl;
	}

	if (input->IsKeyDown(DIK_S))
	{
		D3DXVECTOR3 pos = m_camera->GetPosition();
		pos = pos - m_camera->GetForwardVector() * m_movementSpeed;
		m_camera->SetPosition(pos.x, pos.y, pos.z);
		//std::cout << pos.x << ", " << pos.y << " , " << pos.z << std::endl;

	}

	if (input->IsKeyDown(DIK_Q))
	{
		D3DXVECTOR3 pos = m_camera->GetPosition();
		D3DXVECTOR3 dir = m_camera->GetForwardVector();
		D3DXVECTOR3 up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
		D3DXVec3Cross(&dir, &dir, &up);
		pos = pos + dir * m_movementSpeed;
		m_camera->SetPosition(pos.x, pos.y, pos.z);
		//std::cout << pos.x << ", " << pos.y << " , " << pos.z << std::endl;
	}

	if (input->IsKeyDown(DIK_E))
	{
		D3DXVECTOR3 pos = m_camera->GetPosition();
		D3DXVECTOR3 dir = m_camera->GetForwardVector();
		D3DXVECTOR3 up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
		D3DXVec3Cross(&dir, &dir, &up);
		pos = pos - dir * m_movementSpeed;
		m_camera->SetPosition(pos.x, pos.y, pos.z);
		//std::cout << pos.x << ", " << pos.y << " , " << pos.z << std::endl;
	}

	if (input->IsKeyDown(DIK_D))
	{
		D3DXVECTOR3 rot = m_camera->GetRotation();
		m_camera->SetRotation(rot.x, 1.0f, rot.z);
		//std::cout << pos.x << ", " << pos.y << " , " << pos.z << std::endl;
	}

	if (input->IsKeyDown(DIK_A))
	{
		D3DXVECTOR3 rot = m_camera->GetRotation();
		m_camera->SetRotation(rot.x, -1.0f, rot.z);
		//std::cout << pos.x << ", " << pos.y << " , " << pos.z << std::endl;
	}

}
