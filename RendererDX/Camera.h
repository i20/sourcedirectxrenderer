#pragma once

#include <math.h>
#include <d3dx10math.h>

class Camera
{

#pragma region methods

public:
	Camera(void);
	~Camera(void);

	void InitializeOrthoMatrix(int screenWidth, int screenHeight, float screenNear, float screenDepth);
	void InitializeProjectionMatrix(float fov, float screenAspect, float screenNear, float screenDepth);
	void Update();

#pragma endregion methods

#pragma region getters

	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotation();
	D3DXVECTOR3 GetForwardVector();
	D3DXMATRIX GetViewMatrix();
	D3DXMATRIX GetProjectionMatrix();
	D3DXMATRIX GetOrthoMatrix();

	void SetPosition(float x, float y, float z);
	void SetRotation(float x, float y, float z);

#pragma endregion getters

#pragma region members

private:
	D3DXVECTOR3 m_position;
	D3DXVECTOR3 m_rotation;
	D3DXVECTOR3 m_direction;

	D3DXMATRIX m_viewMatrix;
	D3DXMATRIX m_projectionMatrix;
	D3DXMATRIX m_orthoMatrix;

#pragma endregion members
};


