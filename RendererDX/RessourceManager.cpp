#include "pch.h"

#include <iostream>
#include <d3d11.h>
#include <string>
#include "RessourceManager.h"

#include "Shader.h"
#include "UnlitSpriteShader.h"
#include "Texture.h"
#include "MeshVertexBuffer.h"
#include "Material.h"


RessourceManager* RessourceManager::m_instance = NULL; 


RessourceManager::RessourceManager()
{
	m_hwnd = NULL;
	m_defaultTexture = nullptr;
	m_defaultTexture = nullptr;
	m_defaultGeo = nullptr;
	m_defaulteMaterial = nullptr;
	m_defaultShader = nullptr;
	m_device = nullptr;

}

RessourceManager::~RessourceManager()
{
	while (!m_textures.empty())
	{
		delete m_textures[0];
		m_textures.erase(m_textures.begin());

	}

	while (!m_shaders.empty())
	{
		delete m_shaders[0];
		m_shaders.erase(m_shaders.begin());

	}

}

void RessourceManager::SetDevice(ID3D11Device* device, HWND hwnd)
{
	m_device = device;
	m_hwnd = hwnd;

	// create default texture 
	Texture* tex = new Texture();
	if (!tex->Initialize(m_device))
	{
		delete tex;
		tex = NULL;
		std::cout << "Unable to create default texture  " << std::endl;
		return;
	}

	m_textures.push_back(tex);
}

struct ID3D11Device* RessourceManager::GetDevice()
{
	return m_device;
}

void RessourceManager::LoadShaderRessource(LPCTSTR shaderFileName)
{
	Shader* shader = new UnlitSpriteShader(m_device, m_hwnd, shaderFileName);

	if (!shader->IsInitialized())
	{
		delete shader;
		shader = NULL;
		std::cout << "Unable to initialize " << shaderFileName  << std::endl;
		return;
	}

	m_shaders.push_back(shader);
}

void RessourceManager::LoadTextureRessource(LPCSTR textureFileName)
{
	Texture* tex = new Texture();
	if (!tex->Initialize(m_device, textureFileName))
	{
		delete tex;
		tex = NULL;
		std::cout << "Unable to load " << textureFileName << std::endl;
		return;
	}
	
	m_textures.push_back(tex);
}

void RessourceManager::LoadGeometryRessource(LPCSTR geometryFileName)
{
	MeshVertexBuffer* geo = new MeshVertexBuffer(geometryFileName);
	geo->Initialize(m_device);

	if (!geo->Initialize(m_device))
	{
		delete geo;
		geo = NULL;
		std::cout << "Unable to load "<< geometryFileName << std::endl;
		return;
	}

	m_MeshBuffer.push_back(geo);
	return;
}

class Material* RessourceManager::CreateMaterialRessource(char* materialName)
{
	Material* mat = new Material(materialName, m_device);
	m_materials.push_back(mat);
	return mat;
}

Shader* RessourceManager::GetShaderByName(char* shaderName)
{
	for (int i = 0; i < (int)m_shaders.size(); i++)
	{
		Shader* shader = m_shaders[i];
		std::string resourceShaderName = shader->GetName();

		if (!strcmp(shaderName, resourceShaderName.c_str()))
		{
			return shader;
		}

	}
	std::cout << "could not find shader: " << shaderName << std::endl;

	return GetDefaultShader();
}

Texture* RessourceManager::GetTextureByName(char* textureName)
{
	for (int i = 0; i < (int)m_textures.size(); ++i)
	{
		Texture* tex = m_textures[i];
		std::string resourceShaderName = tex->GetName();

		if (!strcmp(textureName, resourceShaderName.c_str()))
		{
			return tex;
		}

	}
	return GetDefaultTexture();
}

class Material* RessourceManager::GetMaterialByName(char* materialName)
{
	for (int i = 0; i < (int)m_materials.size(); ++i)
	{
		Material* mat = m_materials[i];
		std::string resourceMaterialName = mat->GetName();

		if (!strcmp(materialName, resourceMaterialName.c_str()))
		{
			return mat;
		}

	}
	std::cout << "could not find material : " << materialName << std::endl;
	return GetDefaultMaterial();
}

class MeshVertexBuffer* RessourceManager::GetGeoByName(char* geometryName)
{
	for (int i = 0; i < (int)m_MeshBuffer.size(); ++i)
	{
		MeshVertexBuffer* geo = m_MeshBuffer[i];
		std::string resourceGoeName = geo->GetName();

		if (!strcmp(geometryName, resourceGoeName.c_str()))
		{
			return geo;
		}

	}
	std::cout << "could not find mesh : " << geometryName << std::endl;
	return GetDefaultGeo();
}

class Texture* RessourceManager::GetDefaultTexture()
{
	if (m_defaultTexture != nullptr)
	{
		return m_defaultTexture;
	}

	for (int i = 0; i < (int)m_textures.size(); ++i)
	{
		Texture* tex = m_textures[i];
		std::string resourceShaderName = tex->GetName();

		if (!strcmp("default", resourceShaderName.c_str()))
		{
			m_defaultTexture = tex;
			break;
		}

	}
	return m_defaultTexture;
}

class MeshVertexBuffer* RessourceManager::GetDefaultGeo()
{
	if (m_defaultGeo == nullptr)
	{
		m_defaultGeo = new MeshVertexBuffer((std::string)"");
		m_defaultGeo->Initialize(m_device);
		m_MeshBuffer.push_back(m_defaultGeo);

	}
	return m_defaultGeo;

}

class Material* RessourceManager::GetDefaultMaterial()
{
	if (m_defaulteMaterial == nullptr)
	{
		Material* newMat = new Material("default", m_device);
		newMat->SetShader(GetDefaultShader(),this);
		m_materials.push_back(newMat);
		m_defaulteMaterial = newMat;
	}
	return m_defaulteMaterial;

}

class Shader* RessourceManager::GetDefaultShader()
{
	if (m_defaultShader == nullptr)
	{
		Shader* shader = new Shader(m_device, m_hwnd);

		if (!shader->IsInitialized())
		{
			delete shader;
			shader = NULL;
			std::cout << "Unable create default shader " << std::endl;
			return nullptr;
		}
		m_shaders.push_back(shader);
		m_defaultShader = shader;
	}

	return m_defaultShader;
}

RessourceManager* RessourceManager::GetInstance()
{
	if (m_instance == NULL)
	{
		m_instance = new RessourceManager();
	}
	return m_instance;
}
