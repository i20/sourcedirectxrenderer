#include "pch.h"

#include "Timer.h"
#include "Engine.h"
#include "Graphics.h"
#include "SpriteVertexBuffer.h"
#include "Texture.h"
#include "Material.h"

#include "AnimatedSprite.h"

AnimatedSprite::AnimatedSprite(float size, float frammesPerSecond, float animationSpeed /*= 1.0f*/, bool isLooping /*= true*/) : Sprite(size)
{
	m_texture = nullptr;
	m_rammesPerSecond = (1000.0f / frammesPerSecond) / 1000.0f;
	m_animationSpeed = animationSpeed;
	m_looping = isLooping;
	m_currentSpeed = 0.0f;
	m_deviceContext = nullptr;
	m_currentFrame = 0.0f;
	m_previousFrame = 0.0f;
	m_maxFrames = 0.0f;
}

AnimatedSprite::~AnimatedSprite()
{
	if (m_texture)
	{
		delete m_texture;
		m_texture = nullptr;
	}
}

void AnimatedSprite::Initialise(ID3D11Device* device, ID3D11DeviceContext* deviceContext, Material* material, std::string animatedTexture)
{
	Sprite::Initialise(device,material, true);
	m_deviceContext = deviceContext;
	m_currentFrame = 0;
	m_previousFrame = -1.0f;

	m_texture = m_material->GetMaterialParameter((char*)animatedTexture.c_str());
	if (m_texture == nullptr)
	{
		return;
	}

	if (m_maxFrames < 0.1f)
	{
		m_maxFrames = (float)m_texture->GetWidth() / (float)m_texture->GetHeight();
	}

}

void AnimatedSprite::Render(ID3D11DeviceContext* deviceContext, D3DMATRIX worldMatrix, D3DMATRIX viewMatrix, D3DMATRIX projectionMatrix)
{
	Engine::GetEngine()->GetGraphics()->EnableAlphaBlending(true);
	Sprite::Render(deviceContext, worldMatrix, viewMatrix, projectionMatrix);

}

void AnimatedSprite::Update()
{
	if (m_maxFrames == 1.0f)
	{
		return;
	}

	if (m_currentFrame < m_maxFrames)
	{
		float deltaTime = Timer::GetDeltaTime();
		m_currentSpeed += m_animationSpeed * deltaTime;

		if (m_currentSpeed > m_rammesPerSecond)
		{
			m_currentFrame++;
			m_currentSpeed = 0.0f;
			if (m_currentFrame >= m_maxFrames)
			{
				if (m_looping)
				{
					m_currentFrame = 0.0f;
				}
				else
				{
					m_currentFrame = m_maxFrames;
				}
			}
		}
	}

	// skip update if we use the same frame
	if (m_currentFrame == m_previousFrame)
	{
		return;
	}

	D3D11_MAPPED_SUBRESOURCE mappedRessources;
	VertexBuffer::VertexTypeBase* vertices = ((SpriteVertexBuffer*)m_vertexBuffer)->GetVertices();

	vertices[0].uv.x = m_currentFrame / m_maxFrames;
	vertices[0].uv.y = 1.0f ;

	vertices[1].uv.x = m_currentFrame / m_maxFrames;
	vertices[1].uv.y = 0.0f;

	vertices[2].uv.x = (m_currentFrame + 1.0f) / m_maxFrames;
	vertices[2].uv.y = 0.0f;

	vertices[3].uv.x = ( m_currentFrame +1.0f) / m_maxFrames;
	vertices[3].uv.y = 1.0f;

	HRESULT result = m_deviceContext->Map(m_vertexBuffer->GetVertexBuffer(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedRessources);
	if (FAILED(result))
	{
		return;
	}

	SpriteVertexBuffer::VertexTypeBase* verticesPtr = (SpriteVertexBuffer::VertexTypeBase*)mappedRessources.pData;
	memcpy(verticesPtr, (void*)vertices, sizeof(SpriteVertexBuffer::VertexTypeBase) * m_vertexBuffer->GetVertexCount());

	m_deviceContext->Unmap(m_vertexBuffer->GetVertexBuffer(), 0);

	m_previousFrame = m_currentFrame;

}


