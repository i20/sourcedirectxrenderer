#pragma once

#include <d3d11.h>
#include <D3DX10math.h>

#include "VertexBuffer.h"

class SpriteVertexBuffer : public VertexBuffer
{

#pragma region methods

public:
	SpriteVertexBuffer(float size, bool writable);
	~SpriteVertexBuffer();

	bool Initialize(ID3D11Device* device);

	virtual VertexTypeBase* GetVertices();

#pragma endregion methods

#pragma region members

	float m_size;

#pragma endregion members

};

	