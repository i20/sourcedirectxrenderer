#pragma once

class Engine
{

#pragma region methods

public:
	Engine();
	~Engine();

	bool InitializeGraphics(HWND hwnd);
	bool Initialize(HINSTANCE histance, HWND hwnd);
	void Run();
	void Release();

	static Engine* GetEngine();
	class Graphics* GetGraphics();
	class UserInput* GetInput();

private:
	void Update();
	void Render();

#pragma endregion methods

#pragma region members

private:
	Graphics* m_graphics;

	class CameraController* m_cameraController;
	class Camera* m_camera;
	UserInput* m_input;
	class SceneReader* m_sceneReader;

	class RessourceManager* m_resourceManager;
	class EntityManager* m_entityManager;
	static Engine* m_engineInstance;

#pragma endregion members

};

