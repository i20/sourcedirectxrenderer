#include "pch.h"

#include "SpriteVertexBuffer.h"
#include "Texture.h"
#include "RessourceManager.h"
#include "Shader.h"
#include "Material.h"

#include "Texture.h"

#include "RenderableObject.h"


RenderableObject::~RenderableObject()
{
	if (m_vertexBuffer)
	{
		delete m_vertexBuffer;
		m_vertexBuffer = NULL;
	}

	if (m_shader)
	{
		delete m_shader;
		m_shader = NULL;
	}

	if (m_material)
	{
		delete m_material;
		m_material = NULL;
	}
}

RenderableObject::RenderableObject()
{
	m_vertexBuffer = nullptr;
	m_shader = nullptr;
	m_material = nullptr;
}


void RenderableObject::Initialise(ID3D11Device* device, Material* material)
{
	m_shader = material->GetShader();
	m_material = material;
}


void RenderableObject::Render(ID3D11DeviceContext* deviceContext, D3DMATRIX worldMatrix, D3DMATRIX viewMatrix, D3DMATRIX projectionMatrix)
{

	// bind various buffers to device

	// bind global matrices from shader 
	m_shader->SetShaderGlobalMatrices(deviceContext, worldMatrix, viewMatrix, projectionMatrix);

	// assign uniforms
	m_material->MapRessources(deviceContext);

	m_vertexBuffer->Render(deviceContext);
	m_shader->Render(deviceContext);

	// actual render on screen
	deviceContext->DrawIndexed(m_vertexBuffer->GetIndexCount(), 0, 0);


}
