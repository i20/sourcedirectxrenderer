#pragma once

class FrameWork
{

#pragma region members

private:
	LPCTSTR m_applicationName;
	HINSTANCE m_hInstance;

#pragma endregion 

#pragma region methods

public:
	FrameWork();
	~FrameWork();

	bool Initialize();
	void Run();

	const char * windowsTitle = "DirectX Renderer";

private:

	bool CreateDXWindow(char* windowTitle, int x, int y, int width, int height);
	
#pragma endregion methods 

};

