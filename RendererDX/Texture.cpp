#include "pch.h"
#include "Texture.h"
#include <iostream>

#include < comdef.h >

Texture::Texture()
{
	m_width = 0;
	m_height = 0;
	m_texture = 0;
}


Texture::~Texture()
{
	if (m_texture)
	{
		m_texture->Release();
		m_texture = NULL;
	}

	if (!m_name.empty())
	{
		m_name = "";
	}

}

bool Texture::Initialize(ID3D11Device * device, LPCSTR textureFilename)
{
	HRESULT result;

	result = D3DX11CreateShaderResourceViewFromFile(device, (LPCTSTR)textureFilename, NULL, NULL, &m_texture, NULL);

	if (FAILED(result))
	{
		_com_error err(result);
		LPCTSTR errMsg = err.ErrorMessage();

		if (!Initialize(device))
		{
			return false;
		}
		std::cout << "fail to load texture " << textureFilename << " set to fall back \n";
		std::cout << errMsg << "\n" ;

	}

	std::string texturePath = textureFilename;
	m_name = texturePath;
	size_t pos = m_name.find_last_of("/");
	if (pos >= 0)
	{
		m_name = m_name.substr(pos + 1, m_name.length());
	}

	m_name = m_name.substr(0, m_name.find_last_of("."));

	ID3D11Resource* ressource = 0;
	m_texture->GetResource(&ressource);

	ID3D11Texture2D * texture2D = 0;
	
	result = ressource->QueryInterface(&texture2D);

	if (SUCCEEDED(result))
	{
		D3D11_TEXTURE2D_DESC desc;
		texture2D->GetDesc(&desc);
		m_width = desc.Width;
		m_height = desc.Height;
	}

	texture2D->Release();
	ressource->Release();

	return true;

}

bool Texture::Initialize(ID3D11Device* device)
{

	HRESULT result;

	// odd number to choose a default color
	// exemple used 0xffc99aff
	// set to bright orange color
	static const uint32_t s_pixel = 289000;

	D3D11_SUBRESOURCE_DATA initData = { &s_pixel, sizeof(uint32_t), 0 };

	D3D11_TEXTURE2D_DESC desc = {};
	desc.Width = desc.Height = desc.MipLevels = desc.ArraySize = 1;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_IMMUTABLE;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

	ID3D11Texture2D* tex;
	result = device->CreateTexture2D(&desc, &initData, &tex);

	
	if (!SUCCEEDED(result))
	{
		std::cout << "failed to create default fall back texture \n";
		return false;
	}
	
	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc = {};
	SRVDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SRVDesc.Texture2D.MipLevels = 1;

	result = device->CreateShaderResourceView(tex,&SRVDesc, &m_texture);
	
	tex->Release();
	//ressource->Release();

	if (!SUCCEEDED(result))
	{
		std::cout << "failed to create default fall back texture \n";
		return false;
	}

	m_name = "default";
	m_height = 256;
	m_width = 256;
	
	return true;
}

int Texture::GetWidth()
{
	return m_width;
}

int Texture::GetHeight()
{
	return m_height;
}

std::string Texture::GetName()
{
	return m_name;
}

ID3D11ShaderResourceView* Texture::GetTexture()
{
	return m_texture;
}
