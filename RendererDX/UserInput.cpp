#include "pch.h"
#include "UserInput.h"


UserInput::UserInput()
{
	m_directInput = 0;
	m_keyBoard = 0;
	m_mouse = 0;
	memset(m_keys, 0, sizeof(bool)* s_NumKeys);
}


UserInput::~UserInput()
{
	if (m_mouse)
	{
		m_mouse->Unacquire();
		m_mouse->Release();
		m_mouse = 0;
	}

	if (m_keyBoard)
	{
		m_keyBoard->Unacquire();
		m_keyBoard->Release();
		m_keyBoard = 0;
	}

	if (m_directInput)
	{
		m_directInput->Release();
		m_directInput = 0;
	}

}


bool UserInput::Initialize(HINSTANCE hInstance, HWND hwnd, int screenWidth, int screenHeight)
{
	HRESULT result;

	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	m_mouseX = 0;
	m_mouseY = 0;

	// init mouse
	result = DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_directInput, NULL);
	if (FAILED(result))
	{
		return false;
	}

	// keyboard interface
	result = m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyBoard, NULL);
	if (FAILED(result))
	{
		return false;
	}

	// set data format for keyboard
	result = m_keyBoard->SetDataFormat(&c_dfDIKeyboard);
	if (FAILED(result))
	{
		return false;
	}

	// set cooperative level of the keyboard do it doest share with other programs 
	result = m_keyBoard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE);
	if (FAILED(result))
	{
		return false;
	}

	result = m_keyBoard->Acquire();
	if (FAILED(result))
	{
		return false;
	}

	result = m_directInput->CreateDevice(GUID_SysMouse, &m_mouse, NULL);
	if (FAILED(result))
	{
		return false;
	}

	result = m_mouse->SetDataFormat(&c_dfDIMouse);
	if (FAILED(result))
	{
		return false;
	}

	// set cooperative level of the mouse do it doest share with other programs 
	result = m_mouse->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if (FAILED(result))
	{
		return false;
	}

	result = m_mouse->Acquire();
	if (FAILED(result))
	{
		return false;
	}
	return true;

}

bool UserInput::Update()
{
	memcpy(m_prevKeys, m_keys, sizeof(bool) * s_NumKeys);

	bool result;

	// read keyboard

	result = ReadKeyboard();
	if (!result)
	{
		return false;
	}

	result = ReadMouse();
	if (!result)
	{
		return false;
	}

	// update MousePosition

	ProcessInput();
	return true;

}

bool UserInput::IsKeyDown(unsigned int key)
{
	return m_keys[key];

}

bool UserInput::IsKeyHit(unsigned int key)
{
	return m_keys[key] && !m_prevKeys[key];
}

bool UserInput::GetMousePosition(int& x, int& y)
{
	x = m_mouseX;
	y = m_mouseY;
	return true;
}

bool UserInput::ReadKeyboard()
{
	HRESULT result;

	result = m_keyBoard->GetDeviceState(sizeof(m_keys), (LPVOID)&m_keys);
	if (FAILED(result))
	{
		if ((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			m_keyBoard->Acquire();
		}
		else
		{
			return false;
		}
	}
	return true;

}

bool UserInput::ReadMouse()
{
	// sometimes mouse doest get initialized when putting breakpoint before it reaches its update

	if (m_mouse == NULL)
	{
		m_directInput->CreateDevice(GUID_SysMouse, &m_mouse, NULL);
		return false;
	}

	HRESULT result;

	// read mouse Device

	result = m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&m_mouseState);
	if (FAILED(result))
	{
		if ((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			m_keyBoard->Acquire();
		}
		else
		{
			return false;
		}
	}
	return true;
}

void UserInput::ProcessInput()
{
	m_mouseX += m_mouseState.lX;
	m_mouseY += m_mouseState.lY;

	if (m_mouseX < 0)
	{
		m_mouseX = 0;
	}

	if (m_mouseY < 0)
	{
		m_mouseY = 0;
	}

	if (m_mouseX > m_screenWidth)
	{
		m_mouseX = m_screenWidth;
	}

	if (m_mouseX > m_screenHeight)
	{
		m_mouseX = m_screenHeight;
	}
}
