#pragma once

#include <fstream>

#include <d3d11.h>
#include <D3DX10math.h>

class VertexBuffer
{

public:
	struct VertexTypeBase
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 uv;
		D3DXVECTOR2 uv2;
	};

#pragma region methods

public:
	VertexBuffer();
	virtual ~VertexBuffer();

	virtual bool Initialize(ID3D11Device* device) = 0;
	virtual void Render(ID3D11DeviceContext* deviceContext);
	 
private:
	virtual void SizeOfBuffer();

#pragma endregion methods

#pragma region getters

public:
	virtual ID3D11Buffer* GetVertexBuffer();
	virtual int GetIndexCount();
	virtual int GetVertexCount();
	virtual std::string GetName();

#pragma endregion getters

#pragma region members

protected:

	ID3D11Buffer* m_vertexBuffer;
	int m_vertexCount;

	ID3D11Buffer* m_indexBuffer;
	unsigned long* m_indices;
	int m_indexCount;

	VertexTypeBase* m_vertices;
	void* m_verticesPtr;
	bool m_writable;
	unsigned int m_bufferSize;
	std::string m_name;

#pragma endregion members

};

	