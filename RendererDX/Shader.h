#pragma once

#include <d3d11.h>
#include <D3DX10math.h>
#include <D3DX11async.h>
#include <fstream>
#include <string>
#include <vector>

enum class EConstantUniformType
{
	Texture2D = 0,
	Float = 1,
	Float2 = 2,
	Float3 = 3,
	Float4 = 4,
	Color = 5
};

enum class EshaderType
{
	vertex = 0,
	pixel = 1
};

enum class EinputType
{
	undefinedInputType = 0,
	base = 1,
	standard = 2,
	full = 3
};

struct SUniformParameter
{
public:
	unsigned int bufferPosition;
	EConstantUniformType constantType;
	EshaderType shaderType;
	std::string parameterName;
	class Texture* textureResource;
	std::vector<float> value;
};

class Shader
{

private:

	struct SMatrixBufferType
	{
		D3DXMATRIX worldMatrx;
		D3DXMATRIX viewMatrix;
		D3DXMATRIX projectionMatrix;
	};



#pragma region methods

public:
	Shader(ID3D11Device* device, HWND hwnd, LPCTSTR shaderFileName);
	Shader(ID3D11Device* device, HWND hwnd);

	virtual ~Shader(void);
	virtual void Render(ID3D11DeviceContext* deviceContext);
	bool SetShaderGlobalMatrices(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrx, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix);
	std::vector<SUniformParameter*> GetShaderParameters();

protected:
	virtual bool Initialize(ID3D11Device* device, HWND hwnd, LPCTSTR shaderFileName);
	std::string ConvertToString(LPCTSTR input);

private:
	bool InitializeShader(ID3D11Device* device, HWND hwnd, LPCTSTR shaderFileName);
	void OutputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, LPCTSTR shaderFileName);
	bool ExtractParameters(std::string shaderFileName);
	void ExtactUniform(std::string uniformLine);
	void ExtactTexture2D(std::string uniformLine);
	void RemoveEmptySpaces(std::string &uniformLine);
	bool InitializeDefault(ID3D11Device* device, HWND hwnd);

	D3D11_INPUT_ELEMENT_DESC* CreateBaseDataAssembly(unsigned int *numElement);
	D3D11_INPUT_ELEMENT_DESC* CreateStandardDataAssembly(unsigned int* numElement);

	std::string DefaultShaderString();
#pragma endregion methods

#pragma region getters

public:
	std::string GetName();
	bool IsInitialized();
	ID3D11Device* m_device;


#pragma endregion getters

#pragma region members

protected:
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
	ID3D11Buffer* m_matrixBuffer;
	
	std::string m_shaderName;
	std::string m_fragmentShaderName;
	std::string m_vertexShaderName;
	EinputType m_inputType;
	bool m_initialized;

	std::vector<SUniformParameter*> m_shaderParameters;
	unsigned int m_texture2DIndex;
	

#pragma endregion members

};

