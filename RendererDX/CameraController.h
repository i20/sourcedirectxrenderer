#pragma once

#include <d3d10_1.h>
#include <d3d10.h>
#include <d3d11.h>

class CameraController
{
#pragma region methods

public:
	CameraController();
	~CameraController();

	Camera* Initialize();
	void Update();
	void UpdateInput();

#pragma endregion methods

#pragma region members

private:
	class Camera* m_camera;
	float m_movementSpeed;
	float m_fovAngle;

#pragma endregion members

};

