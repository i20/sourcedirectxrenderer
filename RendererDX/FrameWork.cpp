#include "pch.h"

#include <d3d10_1.h>
#include <d3d10.h>
#include <iostream>

#include "UserInput.h"
#include "Graphics.h"
#include "SystemDefs.h"
#include "Engine.h"

#include "FrameWork.h"



LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

FrameWork::FrameWork()
{

}


FrameWork::~FrameWork()
{

	if (FULLSCREEN)
	{
		ChangeDisplaySettings(NULL, 0);
	}

	Engine::GetEngine()->Release();

	UnregisterClass(m_applicationName, m_hInstance);
	m_hInstance = NULL;
}

bool FrameWork::Initialize()
{

	char *title = const_cast<char *>(windowsTitle);

	if (!CreateDXWindow(title, SCREEN_POSX, SCREEN_POSY, WINDOW_WIDTH, WINDOW_HEIGHT))
	{
		return false;
	}

	//Engine::GetEngine()->SetGameComponent(gameComponent);

	if ( !Engine::GetEngine()->Initialize( m_hInstance, Engine::GetEngine()->GetGraphics()->GetHwnd() ) )
	{
		return false;
	}

	return true;
}

void FrameWork::Run()
{
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	// update loop

	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else 
		{
			Engine::GetEngine()->Run();
		}

	}
}

bool FrameWork::CreateDXWindow(char* windowTitle, int x, int y, int width, int height)
{
	HWND hwnd;
	WNDCLASSEX wc;

	m_applicationName = (LPCTSTR)windowTitle;

	m_hInstance = GetModuleHandle(NULL);

	// setup the windows class with default settings

	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = m_applicationName;
	wc.cbSize = sizeof(WNDCLASSEX);

	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, (LPCTSTR)"RegisterClassEx() failed", (LPCTSTR)"Error", MB_OK);
		return false;
	}


	// TODO: FULLSCREEN 
	
	int screenWidth = GetSystemMetrics(SM_CXSCREEN);
	int screenHeight = GetSystemMetrics(SM_CYSCREEN);
	
	if (FULLSCREEN)
	{
		DEVMODE dmScreenSettings;
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsHeight = (unsigned long)screenWidth;
		dmScreenSettings.dmPelsWidth = (unsigned long)screenHeight;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);
	}
	else
	{
		screenHeight = height;
		screenWidth = width;
	}

	int nStyle = WS_OVERLAPPED | WS_SYSMENU | WS_VISIBLE | WS_CAPTION | WS_MINIMIZEBOX;


	hwnd = CreateWindowEx(WS_EX_APPWINDOW, (LPCTSTR)windowTitle, (LPCTSTR)windowTitle, nStyle, x, y, width, height, NULL, NULL, m_hInstance, NULL);

	if(hwnd == NULL)
	{
		MessageBox(NULL, (LPCTSTR)"CreateWindowEx() failed", (LPCTSTR)"Error", MB_OK);
		Engine::GetEngine()->Release();
		PostQuitMessage(0);
		return false;

	}

	if (!Engine::GetEngine()->InitializeGraphics(hwnd))
	{
		MessageBox(hwnd, (LPCTSTR)"could not initialize DirectX", (LPCTSTR)"Error", MB_OK);
		Engine::GetEngine()->Release();
		PostQuitMessage(0);
		UnregisterClass(m_applicationName, m_hInstance);
		m_hInstance = NULL;
		DestroyWindow(hwnd);

		return false;
	}

	Engine::GetEngine()->GetGraphics()->SetHwnd(hwnd);

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	return true;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	UserInput* input = Engine::GetEngine()->GetInput();

	// if input keyboard is acquired, original way to exit do not work anymore
	if (input != NULL && input->IsKeyDown(DIK_ESCAPE))
	{
		PostQuitMessage(0);
		DestroyWindow(hwnd);

		return 0;
	}

	switch (message)
	{
	case WM_KEYDOWN:
	{
		if (wParam == VK_ESCAPE)
		{
			PostQuitMessage(0);
			DestroyWindow(hwnd);
		}
	}break;
		case WM_PAINT:
		{
			hdc = BeginPaint(hwnd, &ps);
			EndPaint(hwnd, &ps);
		}break;
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			DestroyWindow(hwnd);
		}break;
		default:
		{
			return DefWindowProc(hwnd, message, wParam, lParam);
		}
	};

	return 0;
}