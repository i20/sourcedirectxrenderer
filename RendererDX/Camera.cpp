#include "pch.h"
#include "Camera.h"
#pragma region methods 

Camera::Camera(void)
{
	m_position = D3DXVECTOR3(0, 0, 0);
	m_rotation = D3DXVECTOR3(0, 0, 0);
	m_direction = D3DXVECTOR3(0, 0, 1);
}

Camera::~Camera(void)
{
}

void Camera::InitializeOrthoMatrix(int screenWidth, int screenHeight, float screenNear, float screenDepth)
{
	// Create an orthographic projection matrix for 2D rendering.
	D3DXMatrixOrthoLH(&m_orthoMatrix, (float)screenWidth, (float)screenHeight, screenNear, screenDepth);
}

void Camera::InitializeProjectionMatrix(float fov, float screenAspect, float screenNear, float screenDepth)
{
	// Create the projection matrix for 3D rendering.
	// fov is in radian ...
	D3DXMatrixPerspectiveFovLH(&m_projectionMatrix, fov, screenAspect, screenNear, screenDepth);
}

void Camera::Update()
{
	D3DXVECTOR3 up, lookAt;
	D3DXMATRIX rotationMatrix;

	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, m_rotation.y, m_rotation.x, m_rotation.z);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&m_direction, &m_direction, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);

	// Translate the rotated camera position to the location of the viewer.
	lookAt = m_direction + m_position;

	// Finally create the view matrix from the three updated vectors.
	D3DXMatrixLookAtLH(&m_viewMatrix, &m_position, &lookAt, &up);
	m_rotation = D3DXVECTOR3(0, 0, 0);
}
#pragma endregion methods 

#pragma region getters

D3DXVECTOR3 Camera::GetPosition()
{
	return m_position;
}

D3DXVECTOR3 Camera::GetRotation()
{
	return m_rotation;
}

D3DXVECTOR3 Camera::GetForwardVector()
{
	return m_direction;
}

D3DXMATRIX Camera::GetViewMatrix()
{
	return m_viewMatrix;
}

D3DXMATRIX Camera::GetProjectionMatrix()
{
	return m_projectionMatrix;
}

D3DXMATRIX Camera::GetOrthoMatrix()
{
	return m_orthoMatrix;
}

void Camera::SetPosition(float x, float y, float z)
{
	m_position.x = x;
	m_position.y = y;
	m_position.z = z;
}

void Camera::SetRotation(float x, float y, float z)
{
	m_rotation.x = x * (float)D3DX_PI / 180.0f;
	m_rotation.y = y * (float)D3DX_PI / 180.0f;
	m_rotation.z = z * (float)D3DX_PI / 180.0f;
}

#pragma endregion getters