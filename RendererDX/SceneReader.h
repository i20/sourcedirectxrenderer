#pragma once

#include <string>

class SceneReader
{
#pragma region methods 

public:

	SceneReader();
	~SceneReader();

	void ReadSceneResources(class RessourceManager* resourceManager, class EntityManager* entityManager, std::string path, struct ID3D11DeviceContext* deviceContext );


#pragma endregion methods 

#pragma region members


#pragma endregion members

};

