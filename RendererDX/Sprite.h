#pragma once

#include "RenderableObject.h"

#include <d3d10_1.h>
#include <d3d10.h>
#include <D3DX10math.h>
#include <d3d11.h>

class Sprite : public RenderableObject
{

#pragma region methods

public:
	Sprite(float taretSize);
	virtual ~Sprite(void);

	virtual void Initialise(ID3D11Device* device,class Material* material, bool isWritable = false);
	virtual void Render(ID3D11DeviceContext* deviceContext, D3DMATRIX worldMatrix, D3DMATRIX viewMatrix, D3DMATRIX projectionMatrix) override;

#pragma endregion methods

#pragma region members

protected:
	float m_size; 

#pragma endregion members


};