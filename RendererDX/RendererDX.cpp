
#include "pch.h"

#include <d3d10_1.h>
#include <d3d10.h>

#include <iostream>
#include "FrameWork.h"

int main()
{
    //std::cout << "Hello World!\n"; 

	FrameWork* framework = new FrameWork();

	if (framework->Initialize() )
	{
		framework->Run();
	}

	delete framework;
}

/*
	links :
		https://www.microsoft.com/en-us/download/details.aspx?id=6812

	on Project settings :

		include directories : 
			$(DXSDK_DIR)Include

		library directories :
			$(DXSDK_DIR)Lib\x86;

		Linker :
		dxgi.lib
		d3d11.lib
		d3dx11.lib
		d3dx10.lib
		dinput8.lib
		dxguid.lib
	
	*/