#pragma once

#define DIRECTINPUT_VERSION 0x0800 //remove annoying message

#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include <dinput.h>
#include <dinputd.h>
#include <ntddkbd.h>
#include <ntddmou.h>


class UserInput
{

#pragma region methods 

public:
	
	UserInput();
	~UserInput();

	bool Initialize( HINSTANCE hInstance, HWND hwnd, int screenWidth, int screenHeight );
	bool Update();

	bool IsKeyDown(unsigned int key);
	bool IsKeyHit(unsigned int key);
	bool GetMousePosition(int& x, int& y);

private:

	bool ReadKeyboard();
	bool ReadMouse();
	void ProcessInput();

#pragma endregion methods 

#pragma region members 

private:

	const static int s_NumKeys = 256;

	IDirectInput8 * m_directInput;
	IDirectInputDevice8* m_keyBoard;
	IDirectInputDevice8* m_mouse;
	DIMOUSESTATE m_mouseState; 

	bool m_keys[s_NumKeys];
	bool m_prevKeys[s_NumKeys];

	int m_screenWidth; 
	int m_screenHeight;

	int m_mouseX;
	int m_mouseY;

#pragma endregion members 

};

