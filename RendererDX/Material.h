#pragma once

#include <fstream>
#include <vector>

class Material
{
#pragma region methods 

public:
	Material(std::string materialName, struct ID3D11Device* device);
	~Material(void);

	void SetShader(class Shader* shader, class RessourceManager* manager);

	void SetMaterialParameter(std::string parameterName, class RessourceManager* manager, std::string textureName);
	void SetMaterialParameter(std::string parameterName, float scalar);
	void SetMaterialParameter(std::string parameterName, std::vector<float> vector);
	
	class Texture* GetMaterialParameter(char* textureName);
	class Texture* GetFallBackTexture();
	void CheckTextureValidity(class RessourceManager* manager);
	void MapRessources(struct ID3D11DeviceContext* deviceContext);


private:
	int LookForParameter(std::string parameterName);
	void SetMaterialParameter(std::string parameterName, class Texture* texture);
	void ClearParameters(std::vector<struct SUniformParameter*>);

#pragma endregion methods 

#pragma region getters

public:
	std::string GetName();
	class Shader* GetShader();

#pragma endregion getters


#pragma region members

private:

	class Shader* m_shader;
	ID3D11Device* m_device;
	std::string m_materialName;

	std::vector<struct SUniformParameter*> m_materialParameters;

#pragma endregion members

};

