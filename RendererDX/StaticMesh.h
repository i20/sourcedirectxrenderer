#pragma once

#include "RenderableObject.h"

class StaticMesh :	public RenderableObject
{
#pragma region methods

public:
	StaticMesh();
	~StaticMesh();

	virtual void Initialise(ID3D11Device* device, class Material* recievedShader);
	virtual void SetMeshVertexBuffer(class MeshVertexBuffer* buffer);
	virtual void Render(ID3D11DeviceContext* deviceContext, D3DMATRIX worldMatrix, D3DMATRIX viewMatrix, D3DMATRIX projectionMatrix) override;

#pragma endregion methods

#pragma region members

#pragma endregion members
};

