#include "pch.h"
#include "UnlitSpriteShader.h"


UnlitSpriteShader::UnlitSpriteShader(ID3D11Device* device, HWND hwnd, LPCTSTR shaderFileName) : Shader(device, hwnd, shaderFileName)
{
	m_initialized = InitializeSamplerState(device);
}


UnlitSpriteShader::~UnlitSpriteShader()
{
	if (m_samplerstate) 
	{
		m_samplerstate->Release();
		m_samplerstate = NULL;
	}
}


bool UnlitSpriteShader::Initialize(ID3D11Device* device, HWND hwnd, LPCTSTR shaderFileName)
{
	if (!Shader::Initialize(device, hwnd, shaderFileName))
	{
		return false;
	}

	return true;
}

bool UnlitSpriteShader::InitializeSamplerState(ID3D11Device* device)
{

	D3D11_SAMPLER_DESC samplerDesc;
	HRESULT result;

	samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = -1.0f;
	samplerDesc.MaxAnisotropy = 4;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	result = device->CreateSamplerState(&samplerDesc, &m_samplerstate);
	if (FAILED(result))
	{
		return false;
	}

	return true;
}

void UnlitSpriteShader::Render(ID3D11DeviceContext* deviceContext)
{
	deviceContext->PSSetSamplers(0, 1, &m_samplerstate);
	Shader::Render(deviceContext);

}
