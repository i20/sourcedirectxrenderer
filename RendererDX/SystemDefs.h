#pragma once

#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080

#define WINDOW_WIDTH (SCREEN_WIDTH + 16)
#define WINDOW_HEIGHT (SCREEN_HEIGHT + 38)

#define SCREEN_POSX 80
#define SCREEN_POSY 80

const float SCREEN_DEPTH = 8000.0f;
const float SCREEN_NEAR = 0.1f;
const bool FULLSCREEN = false;
const bool VSYNC = true;