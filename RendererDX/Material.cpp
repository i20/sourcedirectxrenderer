#include "pch.h"
#include <string>
#include "Material.h"
#include <iostream>


#include <d3d10_1.h>
#include <d3d10.h>
#include <D3DX10math.h>
#include <d3d11.h>

#include "Texture.h"
#include "Shader.h"
#include "RessourceManager.h"

Material::Material(std::string materialName, ID3D11Device* device )
{
	m_materialName = materialName;
	m_device = device;
}

Material::~Material(void)
{

}

void Material::SetMaterialParameter(std::string parameterName, RessourceManager* manager, std::string textureName)
{
		SetMaterialParameter(parameterName, manager->GetTextureByName((char*)textureName.c_str()));
}


void Material::SetMaterialParameter(std::string parameterName, class Texture* texture)
{
	int res = LookForParameter(parameterName);
	if (res == -1)
	{
		SUniformParameter* newParam = new SUniformParameter
		{
			(unsigned int)0,
			EConstantUniformType::Texture2D,
			EshaderType::pixel,
			parameterName,
			texture,
			std::vector<float>{}
		};
		m_materialParameters.push_back(newParam);
	}
	else
	{
		SUniformParameter* referenceParam = m_materialParameters.at(res);
		referenceParam->textureResource = texture;
	}
}

void Material::SetMaterialParameter(std::string parameterName, float scalar)
{
	int res = LookForParameter(parameterName);
	if (res == -1)
	{
		SUniformParameter* newParam = new SUniformParameter
		{
			(unsigned int)0,
			EConstantUniformType::Color,
			EshaderType::pixel,
			parameterName,
			nullptr,
			std::vector<float>{scalar}
		};
		m_materialParameters.push_back(newParam);

	}
	else
	{
		SUniformParameter* referenceParam = m_materialParameters.at(res);
		if (referenceParam->value.empty() )
		{
			referenceParam->value.clear();
		}
		referenceParam->value = std::vector<float>{scalar};
	}
}

void Material::SetMaterialParameter(std::string parameterName, std::vector<float> vector)
{
	int res = LookForParameter(parameterName);
	if (res == -1)
	{
		SUniformParameter* newParam = new SUniformParameter
		{
			(unsigned int)0,
			EConstantUniformType::Color,
			EshaderType::pixel,
			parameterName,
			nullptr,
			vector
		};
		m_materialParameters.push_back(newParam);

	}
	else
	{
		SUniformParameter* referenceParam = m_materialParameters.at(res);
		if (referenceParam->value.empty())
		{
			referenceParam->value.clear();
		}
		referenceParam->value = vector;
	}
}

Texture* Material::GetMaterialParameter(char* textureName)
{
	std::string nameRef = textureName;
	Texture* tex = nullptr;

	for (unsigned int Index = 0; Index < m_materialParameters.size(); Index++)
	{
		if (m_materialParameters.at(Index)->constantType == EConstantUniformType::Texture2D)
		{
			tex = m_materialParameters.at(Index)->textureResource;
			if (nameRef.compare(tex->GetName()) == 0)
			{
				return m_materialParameters.at(Index)->textureResource;
			}
		}
	}
	std::cout << "could not find texture parameter on " << m_materialName << "\n";
	return GetFallBackTexture();

	return nullptr;
}


class Texture* Material::GetFallBackTexture()
{
	Texture* tex = nullptr;

	for (unsigned int Index = 0; Index < m_materialParameters.size(); Index++)
	{
		if (m_materialParameters.at(Index)->constantType == EConstantUniformType::Texture2D)
		{
			return m_materialParameters.at(Index)->textureResource;
		}
	}
	std::cout << "could not find fall back texture on "<< m_materialName <<" \n";
	return nullptr;
}


void Material::CheckTextureValidity(RessourceManager* manager)
{
	Texture* tex = nullptr;

	for (unsigned int Index = 0; Index < m_materialParameters.size(); Index++)
	{
		if (m_materialParameters.at(Index)->constantType == EConstantUniformType::Texture2D)
		{
			if (m_materialParameters.at(Index)->textureResource == nullptr)
			{
				m_materialParameters.at(Index)->textureResource = manager->GetDefaultTexture();
			}
		}
	}
}

void Material::MapRessources(ID3D11DeviceContext* deviceContext)
{
	D3DXCOLOR* dataptrColor;
	D3DXVECTOR4* dataptrVec4;
	D3DXVECTOR3* dataptrVec3;
	D3DXVECTOR2* dataptrVec2;
	FLOAT* dataptrFloat;

	for (unsigned int Index = 0; Index < m_materialParameters.size(); Index++)
	{
		SUniformParameter* uniformParam =  m_materialParameters.at(Index);
		if (uniformParam->constantType == EConstantUniformType::Texture2D)
		{
			ID3D11ShaderResourceView* textureResource = uniformParam->textureResource->GetTexture();
			deviceContext->PSSetShaderResources(uniformParam->bufferPosition, 1, &textureResource);
		}
		else 
		{
			HRESULT result;
			D3D11_MAPPED_SUBRESOURCE mappedRessource;
			D3D11_BUFFER_DESC vec_Desc;
			ID3D11Buffer* dataBuffer;

			vec_Desc.Usage = D3D11_USAGE_DYNAMIC;

			// may be unecessary if all buffer must be mmultiple of 16 bytes
			switch (uniformParam->constantType)
			{
			case EConstantUniformType::Float:
				vec_Desc.ByteWidth = sizeof(FLOAT);
				break;
			case EConstantUniformType::Float2:
				vec_Desc.ByteWidth = sizeof(D3DXVECTOR2);
				break;
			case EConstantUniformType::Float3:
				vec_Desc.ByteWidth = sizeof(D3DXVECTOR3);
				break;
			case EConstantUniformType::Float4:
				vec_Desc.ByteWidth = sizeof(D3DXVECTOR4);
				break;
			case EConstantUniformType::Color:
				vec_Desc.ByteWidth = sizeof(D3DXCOLOR);
				break;
			}

			vec_Desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			vec_Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			vec_Desc.MiscFlags = 0;
			vec_Desc.StructureByteStride = 0;

			// create the constant buffer pointer
			result = m_device->CreateBuffer(&vec_Desc, NULL, &dataBuffer);
			if (FAILED(result))
			{
				continue;
			}

			result = deviceContext->Map(dataBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedRessource);
			if (FAILED(result))
			{
				continue;;
			}

			switch (uniformParam->constantType)
			{
			case EConstantUniformType::Float:
				dataptrFloat = (FLOAT*)mappedRessource.pData;
				dataptrFloat = new FLOAT(uniformParam->value[0]);
				break;
			case EConstantUniformType::Float2:
				dataptrVec2 = (D3DXVECTOR2*)mappedRessource.pData;
				dataptrVec2->x = uniformParam->value[0];
				dataptrVec2->y = uniformParam->value[1];
				break;
			case EConstantUniformType::Float3:
				dataptrVec3 = (D3DXVECTOR3*)mappedRessource.pData;
				dataptrVec3->x = uniformParam->value[0];
				dataptrVec3->y = uniformParam->value[1];
				dataptrVec3->z = uniformParam->value[2];
				break;
			case EConstantUniformType::Float4:
				dataptrVec4 = (D3DXVECTOR4*)mappedRessource.pData;
				dataptrVec4->x = uniformParam->value[0];
				dataptrVec4->y = uniformParam->value[1];
				dataptrVec4->z = uniformParam->value[2];
				dataptrVec4->w = uniformParam->value[3];
				break;
			case EConstantUniformType::Color:
				dataptrColor = (D3DXCOLOR*)mappedRessource.pData;
				dataptrColor->r = uniformParam->value[0];
				dataptrColor->g = uniformParam->value[1];
				dataptrColor->b = uniformParam->value[2];
				dataptrColor->a = uniformParam->value[3];
				break;
			}

			deviceContext->Unmap(dataBuffer, 0);

			if (uniformParam->shaderType == EshaderType::pixel)
			{
				deviceContext->PSSetConstantBuffers(uniformParam->bufferPosition, 1, &dataBuffer);
			}
			else 
			{
				deviceContext->VSSetConstantBuffers(uniformParam->bufferPosition, 1, &dataBuffer);

			}
		}
	}
}

void Material::SetShader(class Shader* shader, RessourceManager* manager)
{
	m_shader = shader;
	std::vector<SUniformParameter*> recievedShaderParameterLayout = m_shader->GetShaderParameters();
	std::vector<SUniformParameter*> loadedMaterialsParameters = m_materialParameters;

	for (unsigned int Index = 0; Index < recievedShaderParameterLayout.size(); Index++)
	{
		SUniformParameter* currentRecievedParameter = recievedShaderParameterLayout.at(Index);
		std::string parameterName = currentRecievedParameter->parameterName;
		
		int match = -1;
		for (unsigned int materialIndex = 0; materialIndex < m_materialParameters.size(); materialIndex++)
		{
			std::string loadedParameterName = m_materialParameters.at(materialIndex)->parameterName;
			if (parameterName.compare( loadedParameterName) == 0)
			{
				match = materialIndex;
				break;
			}
		}

		if (match != -1)
		{
			SUniformParameter* existingParam = m_materialParameters[match];

				existingParam->bufferPosition = currentRecievedParameter->bufferPosition;
				existingParam->constantType = currentRecievedParameter->constantType;
				existingParam->shaderType = currentRecievedParameter->shaderType;

			//m_materialParameters.push_back(newParam);
		}
		else 
		{
			// did not find match, apply default values

			std::cout << "could not find material parameter for : " << parameterName << " in : " << m_materialName << " \n ";
			std::vector<float> defaultVec; 
			Texture* defaultTex = nullptr;

			switch (currentRecievedParameter->constantType)
			{
			case EConstantUniformType::Texture2D:
				defaultTex = manager->GetDefaultTexture();
				defaultVec = std::vector<float>{0.0f};
				break;
			case EConstantUniformType::Float:
				defaultVec = std::vector<float>{ 0.0f };
				break;
			case EConstantUniformType::Float2:
				defaultVec = std::vector<float>{ 0.0f, 0.0f };
				break;
			case EConstantUniformType::Float3:
				defaultVec = std::vector<float>{ 0.0f, 0.0f, 0.0f };
				break;
			case EConstantUniformType::Float4:
				defaultVec = std::vector<float>{ 0.0f, 0.0f, 0.0f, 0.0f };
				break;
			case EConstantUniformType::Color:
				defaultVec = std::vector<float>{ 0.0f, 0.0f, 0.0f, 0.0f };
				break;
			default:
				defaultVec = std::vector<float>{ 0.0f };
				break;
			}

			SUniformParameter* newParam = new SUniformParameter
			{
				currentRecievedParameter->bufferPosition,
				currentRecievedParameter->constantType,
				currentRecievedParameter->shaderType,
				parameterName,
				defaultTex,
				defaultVec
			};
			m_materialParameters.push_back(newParam);
		}
	}
}


void Material::ClearParameters(std::vector<struct SUniformParameter*> targetParam)
{
	if (!targetParam.empty())
	{
		for (int Index = 0; Index < (int)targetParam.size(); Index++)
		{
			targetParam.at(Index)->value.clear();
		}
		targetParam.clear();
	}
}

int Material::LookForParameter(std::string parameterName )
{
	int result = -1;
	for (unsigned int i = 0; i < m_materialParameters.size(); i++)
	{
		if (m_materialParameters.at(i)->parameterName.compare(parameterName) == 0  )
		{
			result = i;
			break;
		}
	}
	return result;
}

std::string Material::GetName()
{
	return m_materialName;
}

Shader* Material::GetShader()
{
	return m_shader;
}
