#include "pch.h"

#include "Entity.h"
#include "EntityManager.h"

EntityManager* EntityManager::m_instance = NULL;

EntityManager::EntityManager()
{
}

EntityManager::~EntityManager()
{
	RemoveAllEntities();
}

void EntityManager::Update()
{
	size_t size = m_entities.size();
	for (size_t i = 0; i < size; ++i)
	{
		m_entities[i]->Update();
	}
}

void EntityManager::Render(ID3D11DeviceContext* deviceContext, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix)
{
	size_t size = m_entities.size();
	for (size_t i = 0; i < size; ++i)
	{
		m_entities[i]->Render(deviceContext, viewMatrix, projectionMatrix);
	}
}

Entity* EntityManager::AddEntity()
{
	Entity* entity = new Entity();
	m_entities.push_back(entity);
	return entity;
}

void EntityManager::RemoveEntity(Entity* entity)
{
	if (entity == NULL)
	{
		return;
	}

	int index = -1;

	if (ContainsEntity(entity, index))
	{
		m_entities.erase(m_entities.begin() + index);
		delete entity;
	}
}

void EntityManager::RemoveAllEntities()
{
	while (!m_entities.empty())
	{
		Entity* entity = m_entities[0];
		m_entities.erase(m_entities.begin());
		delete entity;
	}
}

bool EntityManager::ContainsEntity(Entity* entity)
{
	size_t size = m_entities.size();
	for (size_t i = 0; i < size; ++i)
	{
		if (m_entities[i] == entity)
		{
			return true;
		}
	}
	return false;
}

bool EntityManager::ContainsEntity(Entity* entity, int& index)
{
	size_t size = m_entities.size();
	for (size_t i = 0; i < size; ++i)
	{
		if (m_entities[i] == entity)
		{
			index = i;
			return true;
		}
	}
	return false;
}

EntityManager* EntityManager::GetInstance()
{
	if (m_instance == NULL)
	{
		m_instance = new EntityManager();
	}
	return m_instance;
}
