#include "pch.h"

#include "StaticMesh.h"

#include "Shader.h"
#include "MeshVertexBuffer.h"
#include "RessourceManager.h"
#include "Texture.h"
#include "Material.h"

StaticMesh::~StaticMesh()
{
}


StaticMesh::StaticMesh()
{
}

void StaticMesh::Initialise(ID3D11Device* device, Material* material)
{
	RenderableObject::Initialise(device, material);
}

void StaticMesh::SetMeshVertexBuffer(class MeshVertexBuffer* buffer)
{
	m_vertexBuffer = buffer;
}

void StaticMesh::Render(ID3D11DeviceContext* deviceContext, D3DMATRIX worldMatrix, D3DMATRIX viewMatrix, D3DMATRIX projectionMatrix)
{
	RenderableObject::Render(deviceContext, worldMatrix, viewMatrix, projectionMatrix);
}
