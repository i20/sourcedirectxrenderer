#pragma once

#include <d3d11.h>

#include <vector>
#include <iosfwd> 

class RessourceManager
{

#pragma region methods 

public:
	~RessourceManager(void);
	void SetDevice(struct ID3D11Device* device, HWND hwnd );
	struct ID3D11Device* GetDevice();
	void LoadShaderRessource(LPCTSTR shaderFileName);
	void LoadTextureRessource(LPCSTR textureFileName);
	void LoadGeometryRessource(LPCSTR geometryFileName);
	class Material* CreateMaterialRessource(char* materialName);

#pragma endregion methods 

#pragma region getters 

	class Shader* GetShaderByName(char* shaderName);
	class Texture* GetTextureByName(char* textureName);
	class Material* GetMaterialByName(char* materialName);
	class MeshVertexBuffer* GetGeoByName(char* geometryName);
	class Texture* GetDefaultTexture();
	class MeshVertexBuffer* GetDefaultGeo();
	class Material* GetDefaultMaterial();
	class Shader* GetDefaultShader();



	static RessourceManager* GetInstance();

#pragma endregion getters 

#pragma region members

private:

	RessourceManager(void);
	ID3D11Device* m_device;
	HWND m_hwnd;

	std::vector<class Texture*> m_textures;
	std::vector<class Shader*> m_shaders;
	std::vector<class Material*> m_materials;
	std::vector<class MeshVertexBuffer*> m_MeshBuffer;

	static RessourceManager* m_instance;
	class Texture* m_defaultTexture;
	class MeshVertexBuffer* m_defaultGeo;
	class Material* m_defaulteMaterial;
	class Shader* m_defaultShader;

#pragma endregion members

};

