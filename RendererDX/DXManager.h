#pragma once

#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>

class DXManager
{
#pragma region methods 

public:
	DXManager();
	~DXManager();
	
	bool Initialize(int screenWidth, int screenHeight, bool vSync, HWND hwnd, bool fullscreen);
	void BeginScene(float r, float  g, float b, float a);
	void EndScene();
	void EnableAlphaBlending(bool enable);
	void EnableZbuffer(bool enable);

private:

	bool InitializeSwapChainint(HWND hwnd, int screenWidth, int screenHeight, bool fullscreen, unsigned int numerator, unsigned int denumerator, bool vSync);
	bool InitializeDepthBufferTexture(int screenWidth, int screenHeight);
	bool InitializeDepthStencilBuffer();
	bool InitializeStencilView();
	bool InitializeRasterisationState();
	bool InitializeViewPort(int width, int height);
	bool InitializeAlphaBlending();
	bool InitializeZBuffer();

#pragma endregion methods 

#pragma region getters

public:

	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();

#pragma endregion getters

#pragma region members

private:

	bool m_vSyncEnabled;
	int m_videoCardMemory;
	char m_videoCardDescription[128];
	IDXGISwapChain* m_swapChain;
	ID3D11Device* m_device;
	ID3D11DeviceContext* m_deviceContext;
	ID3D11RenderTargetView*  m_renderTargetView;
	ID3D11Texture2D* m_depthStencilBuffer;
	ID3D11DepthStencilState * m_depthStencilState;
	ID3D11DepthStencilView* m_depthstencilView;
	ID3D11RasterizerState* m_rasterizerState;
	ID3D11BlendState* m_alphaEnableBlendingState;
	ID3D11BlendState* m_alphaDisableBlendingState;
	ID3D11DepthStencilState* m_depthDiableStencilState;

#pragma endregion members

};

