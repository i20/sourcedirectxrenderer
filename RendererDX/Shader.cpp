#include "pch.h"
#include "Shader.h"

#include <string.h>
#include <iostream>
#include <algorithm>

//#include < comdef.h >

/*
			_com_error err(result);
			LPCTSTR errMsg = err.ErrorMessage();

			std::cout << "fail to load shader " << shaderFileName << " set to fall back \n";
			std::cout << errMsg << "\n";
*/

Shader::Shader(ID3D11Device* device, HWND hwnd, LPCTSTR shaderFileName)
{
	m_vertexShader = nullptr;
	m_pixelShader = nullptr;
	m_layout = nullptr;
	m_matrixBuffer = nullptr;
	m_shaderParameters = std::vector<SUniformParameter*>();
	m_inputType = EinputType::undefinedInputType;
	m_fragmentShaderName = "";
	m_vertexShaderName = "";
	m_texture2DIndex = 0;
	m_initialized = Initialize(device, hwnd, shaderFileName);
	m_device = device;
}

Shader::Shader(ID3D11Device* device, HWND hwnd)
{
	m_vertexShader = nullptr;
	m_pixelShader = nullptr;
	m_layout = nullptr;
	m_matrixBuffer = nullptr;
	m_shaderParameters = std::vector<SUniformParameter*>();

	m_fragmentShaderName = "";
	m_vertexShaderName = "";
	m_shaderName = "";
	m_texture2DIndex = 0;
	m_initialized = InitializeDefault(device, hwnd);
	m_device = device;
}

Shader::~Shader()
{
	if (m_matrixBuffer)
	{
		m_matrixBuffer->Release();
		m_matrixBuffer = 0;
	}

	if (m_layout)
	{
		m_layout->Release();
		m_layout = 0;
	}

	if (m_pixelShader)
	{
		m_pixelShader->Release();
		m_pixelShader = 0;
	}

	if (m_vertexShader)
	{
		m_vertexShader->Release();
		m_vertexShader = 0;
	}

	if (!m_shaderName.empty() )
	{
		m_shaderName = "";
		
	}

	if (!m_shaderParameters.empty())
	{
		for (int Index = 0; Index < (int)m_shaderParameters.size(); Index++)
		{
			m_shaderParameters.at(Index)->value.clear();
		}
		m_shaderParameters.clear();
	}

}

std::string Shader::ConvertToString(LPCTSTR input)
{

	std::string s;

#ifdef UNICODE
	std::wstring w;
	w = input;
	s = std::string(w.begin(), w.end()); // magic here
#else
	s = input;
#endif
	return s;
}

bool Shader::Initialize(ID3D11Device* device, HWND hwnd, LPCTSTR shaderFileName)
{
	
	std::string shaderPath = ConvertToString(shaderFileName);
	m_shaderName = shaderPath;

	size_t pos = m_shaderName.find_last_of("/");
	if (pos >= 0)
	{
		m_shaderName = m_shaderName.substr(pos + 1, m_shaderName.length());
	}
	
	std::string fileName = ConvertToString(shaderFileName) + ".fx";

	// initialize

	bool fileRead = ExtractParameters(fileName);


	if (!fileRead)
	{
		std::cout << "could not read shader file : " << shaderFileName << "\n";
		return InitializeDefault(device, hwnd);
	}
	
	if (m_inputType == EinputType::undefinedInputType)
	{
		std::cout << shaderFileName << " do not define vertex data input \n";
		return InitializeDefault(device, hwnd);
	}
	
		 
		if (!InitializeShader(device, hwnd, (LPCTSTR)fileName.c_str()))
			return InitializeDefault(device, hwnd);

	return true;

}

bool Shader::IsInitialized()
{
	return m_initialized;
}

void Shader::Render(ID3D11DeviceContext* deviceContext)
{

	deviceContext->IASetInputLayout(m_layout);
	deviceContext->VSSetShader(m_vertexShader, NULL, 0);
	deviceContext->PSSetShader(m_pixelShader, NULL, 0);
}

bool Shader::SetShaderGlobalMatrices(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrx, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedRessource;
	SMatrixBufferType* dataPtr;

	D3DXMatrixTranspose(&worldMatrx, &worldMatrx);
	D3DXMatrixTranspose(&viewMatrix, &viewMatrix);
	D3DXMatrixTranspose(&projectionMatrix, &projectionMatrix);

	// lock buffer
	m_shaderName = m_shaderName;
	result = deviceContext->Map(m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedRessource);
	if (FAILED(result))
	{
		return false;
	}

	dataPtr = (SMatrixBufferType*)mappedRessource.pData;
	dataPtr->worldMatrx = worldMatrx;
	dataPtr->viewMatrix = viewMatrix;
	dataPtr->projectionMatrix = projectionMatrix;
	

	// unlock buffer

	deviceContext->Unmap(m_matrixBuffer, 0);
	//bufferNumber = 0;

	// assign matrix value to video card

	deviceContext->VSSetConstantBuffers(0, 1, &m_matrixBuffer);


	return true; 
}

std::vector<SUniformParameter*> Shader::GetShaderParameters()
{
	return m_shaderParameters;
}

std::string Shader::GetName()
{
	return m_shaderName;
}


bool Shader::InitializeShader(ID3D11Device* device, HWND hwnd, LPCTSTR shaderFileName)
{
	HRESULT result;
	ID3D10Blob* errorMessage = 0;
	ID3D10Blob* vertexShaderbuffer = 0;
	ID3D10Blob* pixelShaderbuffer = 0;
	unsigned int numElements;
	D3D11_BUFFER_DESC matrixBuffer_Desc;


	result = D3DX11CompileFromFile(shaderFileName, NULL, NULL, m_vertexShaderName.c_str() , "vs_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, NULL, &vertexShaderbuffer, &errorMessage, NULL);

	if (FAILED(result))
	{
		if (errorMessage)
		{

			OutputShaderErrorMessage(errorMessage, hwnd, shaderFileName);
		}
		else
		{
			MessageBox(hwnd, shaderFileName, (LPCTSTR)"error in shader file", MB_OK);
		}
		return false;
	}
	
	result = D3DX11CompileFromFile(shaderFileName, NULL, NULL, m_fragmentShaderName.c_str(), "ps_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, NULL, &pixelShaderbuffer, &errorMessage, NULL);

	if (FAILED(result))
	{
		if (errorMessage)
		{
			OutputShaderErrorMessage(errorMessage, hwnd, shaderFileName);
		}
		else
		{
			MessageBox(hwnd, shaderFileName, (LPCTSTR)"error in shader file", MB_OK);
		}
		return false;
	}

	// create actual shader

	result = device->CreateVertexShader(vertexShaderbuffer->GetBufferPointer(), vertexShaderbuffer->GetBufferSize(), NULL, &m_vertexShader);
	if (FAILED(result))
	{
		return false;
	}

	result = device->CreatePixelShader(pixelShaderbuffer->GetBufferPointer(), pixelShaderbuffer->GetBufferSize(), NULL, &m_pixelShader);
	if (FAILED(result))
	{
		return false;
	}

	// setup the layout of the dataBuffer that goes into the shader

	D3D11_INPUT_ELEMENT_DESC *assemblyLayoutDescription = nullptr;
	
	switch (m_inputType)
	{
	default:
		assemblyLayoutDescription = CreateBaseDataAssembly(&numElements);
		break;
	case EinputType::base :
		assemblyLayoutDescription = CreateBaseDataAssembly(&numElements);
		break;
	case EinputType::standard :
		assemblyLayoutDescription = CreateStandardDataAssembly(&numElements);
		break;
	case EinputType::full :
		assemblyLayoutDescription = CreateBaseDataAssembly(&numElements);
		break;
	}


	// create the vertex input layout

	result = device->CreateInputLayout(assemblyLayoutDescription, numElements, vertexShaderbuffer->GetBufferPointer(), vertexShaderbuffer->GetBufferSize(), &m_layout);
	
	if (FAILED(result))
	{

		std::cout << "failed to create input layout on :" << m_shaderName << "\n";
		return false;
	}

	vertexShaderbuffer->Release();
	vertexShaderbuffer = 0;

	pixelShaderbuffer-> Release();
	pixelShaderbuffer = 0;

	matrixBuffer_Desc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBuffer_Desc.ByteWidth = sizeof(SMatrixBufferType);
	matrixBuffer_Desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBuffer_Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBuffer_Desc.MiscFlags = 0;
	matrixBuffer_Desc.StructureByteStride = 0;

	// create the constant buffer pointer
	result = device->CreateBuffer(&matrixBuffer_Desc, NULL, &m_matrixBuffer);
	if (FAILED(result))
	{
		std::cout << "failed to create matrices on :" << m_shaderName << "\n";
		return false;
	}

	return true;
}



bool Shader::ExtractParameters(std::string shaderFileName)
{
	std::ifstream stream;
	std::string VShaderToken = "#define VERTEX_SHADER";
	std::string PShaderToken = "#define FRAGMENT_SHADER";
	std::string inputToken = "#define INPUT";
	std::string uniformToken= "SUNIFORM(";
	std::string defineToken = "#define";
	std::string textureToken = "Texture2D";
	std::string commentToken = "//";

	char emptySpace = *" ";

	stream.open(shaderFileName );
	if (!stream)
	{
		stream.close();
		return false;
	}

	std::string line;
	while (getline(stream, line)) 
	{
		// look for constant buffer 

		std::size_t foundPos = line.find(commentToken);
		if (foundPos != std::string::npos)
		{
			continue;
		}

		foundPos = line.find(uniformToken);
		if (foundPos != std::string::npos)
		{
			size_t uniformFound = foundPos;
			std::size_t foundPos = line.find(defineToken);
			if (foundPos == std::string::npos)
			{
				ExtactUniform(line.substr(max(0, uniformFound - 1), line.length()));
				continue;
			}
		}

		// look for texture buffer 

		foundPos = line.find(textureToken);
		if (foundPos != std::string::npos)
		{
			ExtactTexture2D(line.substr(  foundPos+10, line.length() ) );
			continue;
		}
		
		// look for vertex shader program

		if (m_vertexShaderName.empty())
		{
			foundPos = line.find(VShaderToken);
			if (foundPos != std::string::npos)
			{
				m_vertexShaderName = line.substr(foundPos + 21, line.length());
				RemoveEmptySpaces(m_vertexShaderName);
				continue;
			}
		}

		// look for pixel shader program

		if (m_fragmentShaderName.empty())
		{
			foundPos = line.find(PShaderToken);
			if (foundPos != std::string::npos)
			{
				m_fragmentShaderName = line.substr(foundPos + 23, line.length());
				RemoveEmptySpaces(m_fragmentShaderName);
				continue;
			}
		}

		// look for input type

		if (m_inputType == EinputType::undefinedInputType)
		{
			foundPos = line.find(inputToken);
			if (foundPos != std::string::npos)
			{
				foundPos = line.find("base");
				if (foundPos != std::string::npos)
				{
					m_inputType = EinputType::base;
					continue;
				}
				
				foundPos = line.find("standard");
				if (foundPos != std::string::npos)
				{
					m_inputType = EinputType::standard;
					continue;
				}
				
				foundPos = line.find("full");
				if (foundPos != std::string::npos)
				{
					m_inputType = EinputType::full;
					continue;
				}

			}
		}
		

	}
	stream.close();
	return true;
}

void Shader::ExtactUniform(std::string uniformLine)
{
	int bufferIndex = -1;
	m_texture2DIndex = (unsigned int) 0;
	EshaderType bufferType = EshaderType::vertex;
	std::string uniformType = "";
	std::string uniformName = "";
	
	// find type

	std::string uniformToken = "PSUNIFORM(";
	std::size_t foundPos = uniformLine.find(uniformToken);
	if (foundPos != std::string::npos)
	{
		bufferType = EshaderType::pixel;
	}
	else 
	{
		bufferType = EshaderType::vertex;
	}

	uniformLine = uniformLine.substr(foundPos + 10, uniformLine.length());

	// find number
	
	char currentChar = *"";
	char separator = *",";

	int i = 0;
	while ( currentChar != separator || i == uniformLine.length())
	{
		++i;
		currentChar = uniformLine[i];
	}
	std::string extractedBufferIndex = uniformLine.substr(0, i );
	bufferIndex = std::stoi(extractedBufferIndex);

	uniformLine = uniformLine.substr(i + 1, uniformLine.length());

	// find scalar of vectorType

	i = 0;
	currentChar = *"";

	while (currentChar != separator || i == uniformLine.length())
	{
		++i;
		currentChar = uniformLine[i];
	}
	uniformType = uniformLine.substr(0, i);
	RemoveEmptySpaces(uniformType);

	uniformLine = uniformLine.substr(i + 1, uniformLine.length());

	// find parameter Name;

	i = 0;
	currentChar = *"";
	separator = *")";

	while (currentChar != separator || i == uniformLine.length())
	{
		++i;
		currentChar = uniformLine[i];
	}
	uniformName = uniformLine.substr(0, i);
	RemoveEmptySpaces(uniformName);


	// create actual parameter 

	EConstantUniformType EUniformType = EConstantUniformType::Float;
	if(uniformType =="float")
	{
		EUniformType = EConstantUniformType::Float;
	}
	else
	{
		if (uniformType == "float2")
		{
			EUniformType = EConstantUniformType::Float2;
		}
		else 
		{
			if (uniformType == "float3")
			{
				EUniformType = EConstantUniformType::Float3;

			}
			else
			{
				if (uniformType == "float4")
				{
					EUniformType = EConstantUniformType::Float4;
				}
			}
		}
	}
	std::vector<float> temporatyVec = std::vector<float>{ 0.0f };

	
	SUniformParameter* newParameter = new SUniformParameter
	{
		(unsigned int)bufferIndex,
		EUniformType,
		bufferType,
		uniformName,
		nullptr, 
		temporatyVec
	};
	m_shaderParameters.push_back(newParameter);
}

void Shader::ExtactTexture2D(std::string uniformLine)
{
	RemoveEmptySpaces(uniformLine);
	uniformLine = uniformLine.substr(0, uniformLine.size() - 1);

	std::vector<float> temporatyVec = std::vector<float>{ 0.0f };
	SUniformParameter* newParameter = new SUniformParameter
	{
		m_texture2DIndex,
		EConstantUniformType::Texture2D,
		EshaderType::pixel,
		uniformLine,
		nullptr,
		temporatyVec
	};
	m_shaderParameters.push_back(newParameter);
	++m_texture2DIndex;
}

void Shader::RemoveEmptySpaces(std::string &uniformLine)
{
	char emptySpace = *" ";
	for (int i = uniformLine.length()-1; i > -1; --i)
	{
		if (uniformLine[i] == emptySpace)
		{
			uniformLine.erase(i, 1);
		}
	}
}

bool Shader::InitializeDefault(ID3D11Device* device, HWND hwnd)
{
	HRESULT result;
	ID3D10Blob* errorMessage = 0;
	ID3D10Blob* vertexShaderbuffer = 0;
	ID3D10Blob* pixelShaderbuffer = 0;
	D3D11_INPUT_ELEMENT_DESC* polygonLayout;
	unsigned int numElements;
	D3D11_BUFFER_DESC matrixBuffer_Desc;

	m_vertexShaderName = "vertShader";
	m_fragmentShaderName = "FragmentShader";

	std::string defaultShaderText = DefaultShaderString();
	size_t datasize = defaultShaderText.length();

	result = D3DX11CompileFromMemory(defaultShaderText.c_str(), datasize, nullptr, nullptr, nullptr, m_vertexShaderName.c_str(), "vs_5_0", D3D10_SHADER_ENABLE_STRICTNESS, NULL, 0, &vertexShaderbuffer, &errorMessage, NULL);
	if (FAILED(result))
	{
		return false;
	}
	result = D3DX11CompileFromMemory(defaultShaderText.c_str(), datasize, NULL, NULL, NULL, m_fragmentShaderName.c_str(), "ps_5_0", NULL, NULL, NULL, &pixelShaderbuffer, &errorMessage, NULL);
	if (FAILED(result))
	{
		return false;
	}
	// create actual shader

	result = device->CreateVertexShader(vertexShaderbuffer->GetBufferPointer(), vertexShaderbuffer->GetBufferSize(), NULL, &m_vertexShader);
	if (FAILED(result))
	{
		return false;
	}

	result = device->CreatePixelShader(pixelShaderbuffer->GetBufferPointer(), pixelShaderbuffer->GetBufferSize(), NULL, &m_pixelShader);
	if (FAILED(result))
	{
		return false;
	}

	// setup the layout of the dataBuffer that goes into the shader

	polygonLayout = CreateBaseDataAssembly(&numElements);

	// create the vertex input layout

	result = device->CreateInputLayout(polygonLayout, numElements, vertexShaderbuffer->GetBufferPointer(), vertexShaderbuffer->GetBufferSize(), &m_layout);

	if (FAILED(result))
	{
		return false;
	}

	vertexShaderbuffer->Release();
	vertexShaderbuffer = 0;

	pixelShaderbuffer->Release();
	pixelShaderbuffer = 0;

	matrixBuffer_Desc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBuffer_Desc.ByteWidth = sizeof(SMatrixBufferType);
	matrixBuffer_Desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBuffer_Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBuffer_Desc.MiscFlags = 0;
	matrixBuffer_Desc.StructureByteStride = 0;

	// create the constant buffer pointer
	result = device->CreateBuffer(&matrixBuffer_Desc, NULL, &m_matrixBuffer);
	if (FAILED(result))
	{
		return false;
	}

	return true;
}



std::string Shader::DefaultShaderString()
{
	std::string shaderRaw;

	shaderRaw.append("#define VERTEX_SHADER vertShader \n"
		"#define FRAGMENT_SHADER FragmentShader \n"
		"	cbuffer VS_CONSTANT_BUFFER : register(b0) \n"
		"	{ \n"
		"		matrix worldMatrix; \n"
		"		matrix viewMatrix; \n"
		"		matrix projectionMatrix; \n"
		"	}; \n"
		"	struct vertexInput \n"
		"	{ \n"
		"		float4 position : POSITION; \n"
		"	}; \n"
		"	float4 vertShader(vertexInput input) : SV_POSITION \n"
		"	{ \n"
		"		input.position.w = 1.0; \n"
		"		float4 position = mul(input.position, worldMatrix); \n"
		"		position = mul(position, viewMatrix); \n"
		"		position = mul(position, projectionMatrix); \n"
		"		return position; \n"
		"	} \n"
		"		float4 FragmentShader() : SV_TARGET \n"
		"	{ \n"
		"		return float4(1.0, 0.0, 0.0, 1.0); \n"
		"	} \n" 
	);
	return shaderRaw;
}

void Shader::OutputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, LPCTSTR shaderFileName)
{
	char* compileErrors = (char*)errorMessage->GetBufferPointer();
	size_t bufferSize = errorMessage->GetBufferSize();

	std::ofstream fout;
	fout.open("Shaders/shader-error.txt");

	for (unsigned int i = 0; i < bufferSize; ++i)
	{
		fout << compileErrors[i];
	}

	fout.close();

	errorMessage->Release();
	errorMessage = 0;

	MessageBox(hwnd, (LPCTSTR)"error compiling shader, check shader-error", shaderFileName, MB_OK);

	return;

}

D3D11_INPUT_ELEMENT_DESC* Shader::CreateBaseDataAssembly(unsigned int* numElement)
{
	*numElement = 3;

	D3D11_INPUT_ELEMENT_DESC* inputAssemblyDescription = new D3D11_INPUT_ELEMENT_DESC[*numElement];

	inputAssemblyDescription[0].SemanticName = "POSITION";
	inputAssemblyDescription[0].SemanticIndex = 0;
	inputAssemblyDescription[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputAssemblyDescription[0].InputSlot = 0;
	inputAssemblyDescription[0].AlignedByteOffset = 0;
	inputAssemblyDescription[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputAssemblyDescription[0].InstanceDataStepRate = 0;

	inputAssemblyDescription[1].SemanticName = "TEXCOORD";
	inputAssemblyDescription[1].SemanticIndex = 0;
	inputAssemblyDescription[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	inputAssemblyDescription[1].InputSlot = 0;
	inputAssemblyDescription[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputAssemblyDescription[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputAssemblyDescription[1].InstanceDataStepRate = 0;

	inputAssemblyDescription[2].SemanticName = "TEXCOORD";
	inputAssemblyDescription[2].SemanticIndex = 1;
	inputAssemblyDescription[2].Format = DXGI_FORMAT_R32G32_FLOAT;
	inputAssemblyDescription[2].InputSlot = 0;
	inputAssemblyDescription[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputAssemblyDescription[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputAssemblyDescription[2].InstanceDataStepRate = 0;

	return inputAssemblyDescription;

}

D3D11_INPUT_ELEMENT_DESC* Shader::CreateStandardDataAssembly(unsigned int* numElement)
{
	*numElement = 7;

	D3D11_INPUT_ELEMENT_DESC* inputAssemblyDescription = new D3D11_INPUT_ELEMENT_DESC[*numElement];

	inputAssemblyDescription[0].SemanticName = "POSITION";
	inputAssemblyDescription[0].SemanticIndex = 0;
	inputAssemblyDescription[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputAssemblyDescription[0].InputSlot = 0;
	inputAssemblyDescription[0].AlignedByteOffset = 0;
	inputAssemblyDescription[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputAssemblyDescription[0].InstanceDataStepRate = 0;

	inputAssemblyDescription[1].SemanticName = "TEXCOORD";
	inputAssemblyDescription[1].SemanticIndex = 0;
	inputAssemblyDescription[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	inputAssemblyDescription[1].InputSlot = 0;
	inputAssemblyDescription[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputAssemblyDescription[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputAssemblyDescription[1].InstanceDataStepRate = 0;

	inputAssemblyDescription[2].SemanticName = "TEXCOORD";
	inputAssemblyDescription[2].SemanticIndex = 1;
	inputAssemblyDescription[2].Format = DXGI_FORMAT_R32G32_FLOAT;
	inputAssemblyDescription[2].InputSlot = 0;
	inputAssemblyDescription[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputAssemblyDescription[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputAssemblyDescription[2].InstanceDataStepRate = 0;

	inputAssemblyDescription[3].SemanticName = "NORMAL";
	inputAssemblyDescription[3].SemanticIndex = 0;
	inputAssemblyDescription[3].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputAssemblyDescription[3].InputSlot = 0;
	inputAssemblyDescription[3].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputAssemblyDescription[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputAssemblyDescription[3].InstanceDataStepRate = 0;

	inputAssemblyDescription[4].SemanticName = "TANGENT";
	inputAssemblyDescription[4].SemanticIndex = 0;
	inputAssemblyDescription[4].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputAssemblyDescription[4].InputSlot = 0;
	inputAssemblyDescription[4].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputAssemblyDescription[4].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputAssemblyDescription[4].InstanceDataStepRate = 0;

	inputAssemblyDescription[5].SemanticName = "BINORMAL";
	inputAssemblyDescription[5].SemanticIndex = 0;
	inputAssemblyDescription[5].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputAssemblyDescription[5].InputSlot = 0;
	inputAssemblyDescription[5].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputAssemblyDescription[5].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputAssemblyDescription[5].InstanceDataStepRate = 0;

	inputAssemblyDescription[6].SemanticName = "COLOR";
	inputAssemblyDescription[6].SemanticIndex = 0;
	inputAssemblyDescription[6].Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	inputAssemblyDescription[6].InputSlot = 0;
	inputAssemblyDescription[6].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputAssemblyDescription[6].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputAssemblyDescription[6].InstanceDataStepRate = 0;

	return inputAssemblyDescription;

}