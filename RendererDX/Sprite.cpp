#include "pch.h"

#include "SpriteVertexBuffer.h"
#include "Texture.h"
#include "RessourceManager.h"
#include "Shader.h"
#include "Material.h"

#include "Sprite.h"


Sprite::~Sprite()
{

}

Sprite::Sprite(float taretSize)
{
	m_size = taretSize;
}


void Sprite::Initialise(ID3D11Device* device, Material* material, bool isWritable )
{
	RenderableObject::Initialise(device, material);

	SpriteVertexBuffer* buff = new SpriteVertexBuffer(m_size, isWritable);
	buff->Initialize(device);
	m_vertexBuffer = buff;

}


void Sprite::Render(ID3D11DeviceContext* deviceContext, D3DMATRIX worldMatrix, D3DMATRIX viewMatrix, D3DMATRIX projectionMatrix)
{
	RenderableObject::Render(deviceContext, worldMatrix, viewMatrix, projectionMatrix);
}
