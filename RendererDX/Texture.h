#pragma once

#include <d3d11.h>
#include <D3DX11tex.h>
#include <string>

class Texture
{

#pragma region methods

public:

	Texture();
	~Texture();
	bool Initialize(ID3D11Device * device, LPCSTR filename);
	bool Initialize(ID3D11Device* device);
	ID3D11ShaderResourceView* GetTexture();

#pragma endregion methods

#pragma region members

public:
	int GetWidth();
	int GetHeight();
	std::string GetName();

#pragma endregion members

#pragma region members

private:
	ID3D11ShaderResourceView* m_texture;
	std::string m_name;
	int m_width;
	int m_height;

#pragma endregion members


};

