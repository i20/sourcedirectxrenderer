#pragma once

#include "Shader.h"

class UnlitSpriteShader : public Shader
{
public:

#pragma region methods

	UnlitSpriteShader(ID3D11Device* device, HWND hwnd, LPCTSTR shaderFileName);
	~UnlitSpriteShader();
	void Render(ID3D11DeviceContext* deviceContext) override;

protected:

	bool Initialize(ID3D11Device* device, HWND hwnd, LPCTSTR shaderFileName) override;
	bool InitializeSamplerState(ID3D11Device* device);

#pragma endregion methods

#pragma region members

private:
	ID3D11SamplerState* m_samplerstate;

#pragma endregion members

};

