#pragma once

#include <d3d11.h>
#include <D3DX10math.h>
#include <string>


class Entity
{

#pragma region methods

public:
	friend class EntityManager;

	~Entity();
	void Initialise(ID3D11Device* device, class Material* material, class MeshVertexBuffer* buffer);
	void Initialise(ID3D11Device* device, class Material* material, float spriteSize);
	void Initialise(ID3D11Device* device, ID3D11DeviceContext* deviceContext, Material* material, float spriteSize, float frammesPerSecond, float animationSpeed, std::string animatedTexture = nullptr, bool isLooping = true);

	void Update();
	void Render(ID3D11DeviceContext* deviceContext, D3DMATRIX viewMatrix, D3DMATRIX projectionMatrix);

#pragma endregion methods

#pragma region getters

public:
	void SetPostion(float x = 0.0f, float y = 0.0f, float z = 0.0f);
	void SetPostion(D3DXVECTOR3 vector);
	void SetRotation(float x = 0.0f, float y = 0.0f, float z = 0.0f);
	void SetRotation(D3DXVECTOR3 vector);

#pragma endregion getters

#pragma region members

private:

	Entity();
	D3DXMATRIX m_worldMatrix;
	D3DXVECTOR3 m_position;
	D3DXVECTOR3 m_rotation;

	class RenderableObject* m_renderableObject;

#pragma endregion members

};

