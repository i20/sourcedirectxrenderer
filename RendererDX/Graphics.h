#pragma once

#include "SystemDefs.h"

#include <d3d11.h>
#include <d3d10_1.h>
#include <d3d10.h>

/* this clas soes nothing by itself but it could be usefull to keep in case we try to add another graphic api, then Graphics could choose which version to use*/

class Graphics
{

#pragma region methods

public:
	Graphics();
	~Graphics();
	bool InitializeDX(HWND hwnd);
	void Initialize();
	void BeginScene(float r, float g, float b, float a);
	void EndScene();
	void EnableAlphaBlending( bool enable);
	void EnableZBuffer(bool enable);

#pragma endregion methods

#pragma region Getters

public:
	class DXManager* GetDXManager();
	HWND GetHwnd();
	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();
	void SetHwnd(HWND hwnd);

#pragma endregion Getters

#pragma region members

private:
	DXManager* m_dxManager;
	HWND m_hwnd;

#pragma endregion members
};

